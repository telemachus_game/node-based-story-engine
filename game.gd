extends Control

@export_file("*.md") var file_path = "res://mermaid/story_demo/core.md"
@export_range(20, 200, 1) var button_width_in_chars := 50

@onready var options = [$TabBar/Play/ButtonMargin/Grid/MC0/Option,
		$TabBar/Play/ButtonMargin/Grid/MC1/Option, $TabBar/Play/ButtonMargin/Grid/MC2/Option,
		$TabBar/Play/ButtonMargin/Grid/MC3/Option, $TabBar/Play/ButtonMargin/Grid/MC4/Option,
		$TabBar/Play/ButtonMargin/Grid/MC5/Option, $TabBar/Play/ButtonMargin/Grid/MC6/Option,
		$TabBar/Play/ButtonMargin/Grid/MC7/Option, $TabBar/Play/ButtonMargin/Grid/MC8/Option,
		$TabBar/Play/ButtonMargin/Grid/MC9/Option, $TabBar/Play/ButtonMargin/Grid/MC10/Option,
		$TabBar/Play/ButtonMargin/Grid/MC11/Option, $TabBar/Play/ButtonMargin/Grid/MC12/Option,
		$TabBar/Play/ButtonMargin/Grid/MC13/Option, $TabBar/Play/ButtonMargin/Grid/MC14/Option,
		$TabBar/Play/ButtonMargin/Grid/MC15/Option, $TabBar/Play/ButtonMargin/Grid/MC16/Option,
		$TabBar/Play/ButtonMargin/Grid/MC17/Option, $TabBar/Play/ButtonMargin/Grid/MC18/Option,
		$TabBar/Play/ButtonMargin/Grid/MC19/Option, $TabBar/Play/ButtonMargin/Grid/MC20/Option,
		]

@onready var story_output = $TabBar/Play/TextMargin/Story
@onready var story_edit = $TabBar/Edit/TextMargin/StoryEdit
@onready var option_grid = $TabBar/Play/ButtonMargin/Grid

var story: StoryInterface


func _ready():
	## Create this before we need it. The individual modules will re-init.
	## See StoryInterface._init()
	Print.create_logger("STORY_ENGINE", Print.ERROR, Print.VERBOSE)
	## See MermaidParser._init()
	Print.create_logger("MERMAID_PARSER", Print.ERROR, Print.VERBOSE)
	_load_story()


func _on_option_pressed(index):
	update_node(index)


func update_node(index):
	if story.options[index] != "":
		story_output.append_text("[right]" + story.options[index] + "[/right]\n")
	story.advance(index)
	_update_text()


func _update_text():
	story_output.append_text("[left]" + story.text + "[/left]\n")
	var longest_text = 0
	var story_options = story.options
	
	for i in options.size():
		if i < story_options.size():
			if story_options[i] == "":
				options[i].text = "Continue"
			else:
				options[i].text = story_options[i]
			options[i].visible = true
			if story.options[i].length() > longest_text:
				longest_text = story.options[i].length()
		else:
			options[i].visible = false
	
	# Set the width based on the longest sentence.
	@warning_ignore("integer_division")
	option_grid.columns = clamp(3 - (longest_text / button_width_in_chars), 1, 3)


func _load_story():
	Print.from("STORY_ENGINE", "Loading Story from file: " + file_path, Print.INFO)
	story = StoryInterface.new(MermaidParser.load_from_file(file_path))
	story_output.clear()
	_update_text()


func _on_story_meta_clicked(meta):
	OS.shell_open(str(meta))


func _on_reload_pressed():
	_load_story()


func _on_save_pressed():
	pass # Replace with function body.


func _on_load_pressed():
	pass # Replace with function body.
