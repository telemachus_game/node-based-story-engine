# GdUnit generated TestSuite
class_name MermaidParserFileTest
extends GdUnitTestSuite


var iterations := 100


func before():
	Print.silence_all()


func test_load_from_file():
	# Generate a flowchart
	var flow_gen := FlowchartGenerator.new()
	flow_gen.generate_flowchart(3)
	
	# Save the flowchart to a temp file
	var temp_dir := create_temp_dir("test/mermaid")
	var file_to_save := temp_dir + "/mermaid_flowchart.txt"
	
	var file := create_temp_file("test/mermaid", "mermaid_flowchart.txt")
	assert_object(file).is_not_null()
	# write some example data
	file.store_line(flow_gen.markdown)
	file.close()
	
	# Load and parse the flowchart from the temp file
	var mermaid = MermaidParser.load_from_file(file_to_save)
	
	# Check the result
	assert_object(mermaid).is_not_null()
	if mermaid:
		flow_gen.assert_equals(mermaid)
		assert_str(mermaid.compose()).is_equal(flow_gen.flowchart)
	
	# Clean up
	flow_gen.queue_free()


func test_load_from_non_markdown():
	# Generate a flowchart
	var flow_gen := FlowchartGenerator.new()
	flow_gen.generate_flowchart(3)
	
	# Save the flowchart to a temp file
	var temp_dir := create_temp_dir("test/mermaid")
	var file_to_save := temp_dir + "/mermaid_flowchart.txt"
	
	var file := create_temp_file("test/mermaid", "mermaid_flowchart.txt")
	assert_object(file).is_not_null()
	# write some example data
	file.store_line(flow_gen.flowchart)
	file.close()
	
	# Load and parse the flowchart from the temp file
	var mermaid = MermaidParser.load_from_file(file_to_save)
	
	# Check the result
	assert_object(mermaid).is_not_null()
	if mermaid:
		flow_gen.assert_equals(mermaid)
		assert_str(mermaid.compose()).is_equal(flow_gen.flowchart)
	
	# Clean up
	flow_gen.queue_free()


func test_invalid_file():
	Print.clear_errors()
	
	# Load and parse the flowchart from the temp file
	var _mermaid = MermaidParser.load_from_file("InvalidFile.md")
	
	assert_int(Print.error_count).is_equal(1)


func test_empty_file():
	# Save noting flowchart to a temp file
	var temp_dir := create_temp_dir("test/mermaid")
	var file_to_save := temp_dir + "/mermaid_flowchart.txt"
	
	var file := create_temp_file("test/mermaid", "mermaid_flowchart.txt")
	assert_object(file).is_not_null()
	# write some example data
	file.store_line("")
	file.close()
	
	# Load and parse the flowchart from the temp file
	var mermaid = MermaidParser.load_from_file(file_to_save)
	
	# Check the result
	assert_object(mermaid).is_null()
	assert_int(Print.error_count).is_equal(1)


func test_corrupted_file():
	# Save noting flowchart to a temp file
	var temp_dir := create_temp_dir("test/mermaid")
	var file_to_save := temp_dir + "/mermaid_flowchart.txt"
	var gen = FlowchartGenerator.new()
	
	var file := create_temp_file("test/mermaid", "mermaid_flowchart.txt")
	assert_object(file).is_not_null()
	# write some example data
	file.store_line(gen.random_string(randi_range(1, 100)))
	file.close()
	
	# Load and parse the flowchart from the temp file
	var mermaid = MermaidParser.load_from_file(file_to_save)
	
	# Check the result
	assert_object(mermaid).is_null()
	assert_int(Print.error_count).is_equal(1)
	
	# Clean up
	gen.queue_free()
