# GdUnit generated TestSuite
class_name MermaidParserSubgraphTest
extends GdUnitTestSuite


func before():
	Print.silence_non_error_printing()


func before_test():
	Print.start_all()


func after_test():
	if is_failure():
		Print.from("MERMAID_PARSER", "Test failed! Dumping message history")
		Print.from("STORY_ENGINE", "Test failed! Dumping message history")


@warning_ignore("unused_parameter")
func test_parse_submodule_from_file(nodes: Dictionary, errors: int, test_parameters := [
		[{"start": MermaidNode.NodeType.COMMAND, "id1": MermaidNode.NodeType.DIALOGUE, "exit": MermaidNode.NodeType.COMMAND}, 0],
		[{"start": MermaidNode.NodeType.DIALOGUE, "id1": MermaidNode.NodeType.DIALOGUE, "exit": MermaidNode.NodeType.COMMAND}, 0],
		[{"id0": MermaidNode.NodeType.DIALOGUE, "id1": MermaidNode.NodeType.DIALOGUE, "exit": MermaidNode.NodeType.COMMAND}, 0],
		[{"start": MermaidNode.NodeType.COMMAND, "id1": MermaidNode.NodeType.DIALOGUE, "exit": MermaidNode.NodeType.DIALOGUE}, 1],
		[{"start": MermaidNode.NodeType.COMMAND, "id1": MermaidNode.NodeType.DIALOGUE, "id2": MermaidNode.NodeType.COMMAND}, 1],
		[{"id0": MermaidNode.NodeType.DIALOGUE, "start": MermaidNode.NodeType.COMMAND, "id1": MermaidNode.NodeType.DIALOGUE, "exit": MermaidNode.NodeType.DIALOGUE}, 1],
		[{"exit": MermaidNode.NodeType.COMMAND}, 0],
		[{"exit": MermaidNode.NodeType.DIALOGUE}, 1],
		]):
	Print.clear_errors()
	Print.silence_all()
	
	# Generate a custom flowchart with a submodule node
	var flow_gen := FlowchartGenerator.new()
	flow_gen.begin_chart("a", "node1")
	flow_gen.chain_node("b", "node2")
	flow_gen.chain_node("sg", "subgraph.md", MermaidNode.NodeType.SUBGRAPH)
	flow_gen.chain_node("c", "node3")
	
	var file := create_temp_file("test/mermaid", "mermaid_flowchart.md")
	assert_object(file).is_not_null()
	# write the custom flowchart
	file.store_line("#A flowchart containing a submodule file.\n" + flow_gen.markdown)
	file.close()

	# Create a subgraph file
	var subgraph_gen := FlowchartGenerator.new()
	subgraph_gen.begin_chart(nodes.keys()[0], subgraph_gen.random_string(10), nodes.values()[0])
	for i in range(1, nodes.size()):
		subgraph_gen.chain_node(nodes.keys()[i], subgraph_gen.random_string(10), nodes.values()[i])
	
	var subgraph_file := create_temp_file("test/mermaid", "subgraph.md")
	assert_object(subgraph_file).is_not_null()
	# write some data to the subgraph file
	subgraph_file.store_line(subgraph_gen.markdown)
	subgraph_file.close()
	
	# Load and parse the flowchart from the temp file
	var mermaid = MermaidParser.load_from_file("user://tmp/test/mermaid/mermaid_flowchart.md")
	
	# Check the result
	assert_object(mermaid).is_not_null()
	if mermaid:
		flow_gen.assert_equals(mermaid)
		assert_str(flow_gen.flowchart).is_equal(mermaid.compose())
	
	var sg_node = mermaid.nodes["sg"]
	if sg_node._mermaid:
		subgraph_gen.assert_equals(sg_node._mermaid)
		assert_str(sg_node._mermaid.compose()).is_equal(subgraph_gen.flowchart)
	
	# Make sure that all errors resolved:
	assert_int(Print.error_count).is_equal(errors)
	
	# Clean up
	flow_gen.queue_free()
	subgraph_gen.queue_free()


@warning_ignore("unused_parameter")
func test_parse_submodule_from_text(nodes: Dictionary, errors: int, test_parameters := [
		[{"start": MermaidNode.NodeType.COMMAND, "id1": MermaidNode.NodeType.DIALOGUE, "exit": MermaidNode.NodeType.COMMAND}, 0],
		[{"start": MermaidNode.NodeType.DIALOGUE, "id1": MermaidNode.NodeType.DIALOGUE, "exit": MermaidNode.NodeType.COMMAND}, 0],
		[{"id0": MermaidNode.NodeType.DIALOGUE, "id1": MermaidNode.NodeType.DIALOGUE, "exit": MermaidNode.NodeType.COMMAND}, 0],
		[{"start": MermaidNode.NodeType.COMMAND, "id1": MermaidNode.NodeType.DIALOGUE, "exit": MermaidNode.NodeType.DIALOGUE}, 1],
		[{"start": MermaidNode.NodeType.COMMAND, "id1": MermaidNode.NodeType.DIALOGUE, "id2": MermaidNode.NodeType.COMMAND}, 1],
		[{"id0": MermaidNode.NodeType.DIALOGUE, "start": MermaidNode.NodeType.COMMAND, "id1": MermaidNode.NodeType.DIALOGUE, "exit": MermaidNode.NodeType.DIALOGUE}, 1],
		[{"exit": MermaidNode.NodeType.COMMAND}, 0],
		[{"exit": MermaidNode.NodeType.DIALOGUE}, 1],
		]):
	Print.clear_errors()
	
	# Create a subgraph node's text
	var subgraph_gen := FlowchartGenerator.new()
	subgraph_gen.begin_chart(nodes.keys()[0], subgraph_gen.random_string(10), nodes.values()[0])
	for i in range(1, nodes.size()):
		subgraph_gen.chain_node(nodes.keys()[i], subgraph_gen.random_string(10), nodes.values()[i])

	# Generate a custom flowchart with a submodule node
	var flow_gen := FlowchartGenerator.new()
	flow_gen.begin_chart("a", "node1")
	flow_gen.chain_node("b", "node2")
	flow_gen.chain_node("sg", "\"" + subgraph_gen.flowchart + "\"", MermaidNode.NodeType.SUBGRAPH)
	flow_gen.chain_node("c", "node3")
	
	var file := create_temp_file("test/mermaid", "mermaid_flowchart.md")
	assert_object(file).is_not_null()
	# write the custom flowchart
	file.store_line("## A flowchart with an inline subgraph.\n" + flow_gen.markdown)
	file.close()
	
	# Load and parse the flowchart from the temp file
	var mermaid = MermaidParser.load_from_file("user://tmp/test/mermaid/mermaid_flowchart.md")
	
	# Check the result
	assert_object(mermaid).is_not_null()
	if mermaid:
		flow_gen.assert_equals(mermaid)
		assert_str(flow_gen.flowchart).is_equal(mermaid.compose())
	
	# Make sure that all errors resolved:
	assert_int(Print.error_count).is_equal(errors)
	
	# Clean up
	flow_gen.queue_free()
	subgraph_gen.queue_free()


@warning_ignore("unused_parameter")
func test_parse_mermaid_submodule(nodes: Dictionary, errors: int, test_parameters := [
		[{"start": MermaidNode.NodeType.COMMAND, "id1": MermaidNode.NodeType.DIALOGUE, "exit": MermaidNode.NodeType.COMMAND}, 0],
		[{"start": MermaidNode.NodeType.DIALOGUE, "id1": MermaidNode.NodeType.DIALOGUE, "exit": MermaidNode.NodeType.COMMAND}, 0],
		[{"id0": MermaidNode.NodeType.DIALOGUE, "id1": MermaidNode.NodeType.DIALOGUE, "exit": MermaidNode.NodeType.COMMAND}, 0],
		[{"start": MermaidNode.NodeType.COMMAND, "id1": MermaidNode.NodeType.DIALOGUE, "exit": MermaidNode.NodeType.DIALOGUE}, 0],
		[{"start": MermaidNode.NodeType.COMMAND, "id1": MermaidNode.NodeType.DIALOGUE, "id2": MermaidNode.NodeType.COMMAND}, 0],
		[{"id0": MermaidNode.NodeType.DIALOGUE, "start": MermaidNode.NodeType.COMMAND, "id1": MermaidNode.NodeType.DIALOGUE, "exit": MermaidNode.NodeType.DIALOGUE}, 0],
		[{"exit": MermaidNode.NodeType.COMMAND}, 0],
		]):
	Print.clear_errors()
	
	# Generate a custom flowchart with a submodule node
	var flow_gen := FlowchartGenerator.new()
	flow_gen.begin_chart("a", "node1")
	flow_gen.chain_node("b", "node2")
	flow_gen.start_subgraph("sg")
	# Create a subgraph node's text
	var subgraph_gen := FlowchartGenerator.new()
	subgraph_gen.begin_chart(nodes.keys()[0], subgraph_gen.random_string(10), nodes.values()[0])
	flow_gen.add_node(nodes.keys()[0], subgraph_gen.random_string(10), nodes.values()[0])
	for i in range(1, nodes.size()):
		subgraph_gen.chain_node(nodes.keys()[i], subgraph_gen.random_string(10), nodes.values()[i])
		flow_gen.chain_node(nodes.keys()[i], subgraph_gen.random_string(10), nodes.values()[i])
	
	flow_gen.end_subgraph()
	flow_gen.chain_node("c", "node3")
	
	var file := create_temp_file("test/mermaid", "mermaid_flowchart.md")
	assert_object(file).is_not_null()
	# write the custom flowchart
	file.store_line("## A flowchart with an inline subgraph.\n" + flow_gen.markdown)
	file.close()
	
	# Load and parse the flowchart from the temp file
	var mermaid = MermaidParser.load_from_file("user://tmp/test/mermaid/mermaid_flowchart.md")
	
	# Check the result
	assert_object(mermaid).is_not_null()
	if mermaid:
		flow_gen.assert_equals(mermaid)
		assert_str(mermaid.compose()).is_equal(flow_gen.flowchart)
	
	# Make sure that all errors resolved:
	assert_int(Print.error_count).is_equal(errors)
	
	# Clean up
	flow_gen.queue_free()
	subgraph_gen.queue_free()

