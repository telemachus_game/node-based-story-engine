# GdUnit generated TestSuite
class_name MermaidNodeTest
extends GdUnitTestSuite
@warning_ignore('unused_parameter')
@warning_ignore('return_value_discarded')


func before():
	Print.silence_non_error_printing()


func before_test():
	Print.start_all()


func after_test():
	if is_failure():
		Print.from("MERMAID_PARSER", "Test failed! Dumping message history")


## Tests the functionality of adding unidirectional links between nodes.
func test_add_link() -> void:
	# create nodes
	var node1: MermaidNode = auto_free(MermaidNode.new("1", "Node 1"))
	var node2: MermaidNode = auto_free(MermaidNode.new("2", "Node 2"))
	var node3: MermaidNode = auto_free(MermaidNode.new("3", "Node 3"))

	# add links
	node1.add_link(node2, "Link 1 to 2", MermaidLink.LinkType.SOLID, MermaidLink.LinkDirection.UNIDIRECTIONAL, MermaidLink.ArrowType.ARROW)
	node2.add_link(node3, "Link 2 to 3", MermaidLink.LinkType.SOLID, MermaidLink.LinkDirection.UNIDIRECTIONAL, MermaidLink.ArrowType.ARROW)

	_auto_free_node(node1)
	_auto_free_node(node2)
	_auto_free_node(node3)
	
	# test node1 links
	assert_int(node1.links.size()).is_equal(1)
	assert_object(node1.links[0].node).is_same(node2)

	# test node2 links
	assert_int(node2.links.size()).is_equal(1)
	assert_object(node2.links[0].node).is_same(node3)

	# test node3 has no outgoing links
	assert_int(node3.links.size()).is_equal(0)


## Tests the functionality of adding bidirectional links between nodes.
func test_add_bidirectional_link() -> void:
	var node1: MermaidNode = auto_free(MermaidNode.new("1", "Node 1"))
	var node2: MermaidNode = auto_free(MermaidNode.new("2", "Node 2"))

	# Add a bidirectional link
	node1.add_link(node2, "Link 1 to 2", MermaidLink.LinkType.SOLID, MermaidLink.LinkDirection.BIDIRECTIONAL, MermaidLink.ArrowType.ARROW)
	
	_auto_free_node(node1)
	_auto_free_node(node2)
	
	assert_int(node1.links.size()).is_equal(1)
	assert_object(node1.links[0].node).is_same(node2)

	assert_int(node2.links.size()).is_equal(1)
	assert_object(node2.links[0].node).is_same(node1)


## Tests the functionality of a node having multiple outgoing links.
func test_add_multiple_links() -> void:
	var node1: MermaidNode = auto_free(MermaidNode.new("1", "Node 1"))
	var node2: MermaidNode = auto_free(MermaidNode.new("2", "Node 2"))
	var node3: MermaidNode = auto_free(MermaidNode.new("3", "Node 3"))

	# Add multiple links
	node1.add_link(node2, "Link 1 to 2", MermaidLink.LinkType.SOLID, MermaidLink.LinkDirection.UNIDIRECTIONAL, MermaidLink.ArrowType.ARROW)
	node1.add_link(node3, "Link 1 to 3", MermaidLink.LinkType.DOTTED, MermaidLink.LinkDirection.UNIDIRECTIONAL, MermaidLink.ArrowType.CROSS)

	_auto_free_node(node1)
	_auto_free_node(node2)
	_auto_free_node(node3)

	assert_int(node1.links.size()).is_equal(2)
	# test the links in node1
	assert_object(node1.links[0].node).is_same(node2)
	assert_object(node1.links[1].node).is_same(node3)


## Tests the functionality of a node linking to itself.
func test_self_reference_link() -> void:
	var node1: MermaidNode = auto_free(MermaidNode.new("1", "Node 1"))

	# Add a link to self
	node1.add_link(node1, "Link 1 to 1", MermaidLink.LinkType.SOLID, MermaidLink.LinkDirection.UNIDIRECTIONAL, MermaidLink.ArrowType.ARROW)
	
	_auto_free_node(node1)
	
	assert_int(node1.links.size()).is_equal(1)
	assert_object(node1.links[0].node).is_same(node1)


## Tests the functionality of adding a link with no note specified.
func test_add_link_no_text() -> void:
	var node1: MermaidNode = auto_free(MermaidNode.new("1"))
	var node2: MermaidNode = auto_free(MermaidNode.new("2"))

	# Add a link with no note specified
	node1.add_link(node2, "", MermaidLink.LinkType.SOLID, MermaidLink.LinkDirection.UNIDIRECTIONAL, MermaidLink.ArrowType.ARROW)
	
	_auto_free_node(node1)
	
	assert_int(node1.links.size()).is_equal(1)
	assert_object(node1.links[0].node).is_same(node2)
	assert_str(node1.links[0].note).is_equal("")


func _auto_free_node(node: MermaidNode):
	auto_free(node)
	for link in node.links:
		auto_free(link)
