# GdUnit generated TestSuite
class_name MermaidParserBasicTest
extends GdUnitTestSuite


func before():
	Print.silence_non_error_printing()


func before_test():
	Print.start_all()


func after_test():
	if is_failure():
		Print.from("MERMAID_PARSER", "Test failed! Dumping message history")


@warning_ignore("unused_parameter")
func test_is_valid_mermaid_flowchart_simple(input_string: String, expected_result: bool, test_parameters := [
		["flowchart TD\nidA-->B", true],
		["graph TD\nA-->B", true],
		["flowchart LR\nA-->B", true],
		["sequenceDiagram\nA->>B", false],
		["flowchart LR\nid", true],
	]) -> void:
	assert_bool(MermaidParser.is_string_valid_mermaid(input_string)).is_equal(expected_result)


@warning_ignore("unused_parameter")
func test_is_valid_mermaid_flowchart_all(input_string: String, expected_result: bool, test_parameters := [
		["flowchart LR\nid1[This is the text in the box]", true],
		["flowchart LR\nid[\"This ❤ Unicode\"]", true],
		["flowchart LR\nmarkdown[\"This **is** _Markdown_\"]\nnewLines[\"Line1\nLine 2\nLine 3\"]\nmarkdown --> newLines", true],
		["flowchart TD\nStart --> Stop", true],
		["flowchart LR\nStart --> Stop", true],
		["flowchart LR\nid1(This is the text in the box)", true],
		["flowchart LR\nid1([This is the text in the box])", true],
		["flowchart LR\nid1[[This is the text in the box]]", true],
		["flowchart LR\nid1[(Database)]", true],
		["flowchart LR\nid1((This is the text in the circle))", true],
		["flowchart LR\nid1{This is the text in the box}", true],
		["flowchart LR\nid1{{This is the text in the box}}", true],
		["flowchart TD\nid1[/This is the text in the box/]", true],
		["flowchart TD\nid1[\\This is the text in the box\\]", true],
		["flowchart TD\nA[/Christmas\\]", true],
		["flowchart TD\nB[\\Go shopping\\]", true],
		["flowchart TD\nid1(((This is the text in the circle)))", true],
		["flowchart LR\nA-->B", true],
		["flowchart LR\nA --- B", true],
		["flowchart LR\nA-- This is the text! ---B", true],
		["flowchart LR\nA---|This is the text|B", true],
		["flowchart LR\nA-->|text|B", true],
		["flowchart LR\nA-- text -->B", true],
		["flowchart LR\nA-.->B;", true],
		["flowchart LR\nA-. text .-> B", true],
		["flowchart LR\nA ==> B", true],
		["flowchart LR\nA == text ==> B", true],
		["flowchart LR\nA ~~~ B", true],
		["flowchart LR\nA -- text --> B -- text2 --> C", true],
		["flowchart LR\na --> b & c--> d", true],
		["flowchart TB\nA & B--> C & D", true],
		["flowchart TB\nA --> C\nA --> D\nB --> C\nB --> D", true],
		["flowchart LR\nA --o B\nB --x C", true],
		["flowchart LR\nA o--o B\nB <--> C\nC x--x D", true],
		["flowchart TD\nA[Start] --> B{Is it?}\nB -->|Yes| C[OK]\nC --> D[Rethink]\nD --> B\nB ---->|No| E[End]", true],
		["flowchart TD\nA[Start] --> B{Is it?}\nB -- Yes --> C[OK]\nC --> D[Rethink]\nD --> B\nB -- No ----> E[End]", true],
		["flowchart LR\nid1[\"This is the (text) in the box\"]", true],
		["flowchart LR\nid1[\"This is the (text) in the box\"]", true],
		["flowchart LR\nA[\"A double quote:#quot;\"] -->B[\"A dec char:#9829;\"]", true],
		["flowchart LR\nA[\"Werds\"] -->B[\"More Werds\"]", true],
		["flowchart LR\nid1>This is the text in the box]", true],
		]) -> void:
	assert_bool(MermaidParser.is_string_valid_mermaid(input_string)).is_equal(expected_result)


@warning_ignore("unused_parameter")
func test_is_valid_mermaid_flowchart_failing(input_string: String, expected_result: bool, test_parameters := [
		# The direction specifier is missing
		["flowchart\nid --> id2", false],
		# The direction LR (left-to-right) or TD (top-down) must be specified
		["flowchart\nid --> id2", false],
	]) -> void:
	assert_bool(MermaidParser.is_string_valid_mermaid(input_string)).is_equal(expected_result)


@warning_ignore("unused_parameter")
func test_compose_basic(
		nodes: Dictionary, 
		links: Array, 
		orientation: String, 
		expected: String, 
		test_parameters = [
			[{"A": [MermaidNode.NodeType.STANDARD, ""]}, [], "TB", "flowchart TB\nA\n"],
			[{"A": [MermaidNode.NodeType.STANDARD, ""], "B": [MermaidNode.NodeType.STANDARD, ""]}, [["A", "B"]], "TB", "flowchart TB\nA-->B\n"],
			[{"A": [MermaidNode.NodeType.DIALOGUE, "Node A"], "B": [MermaidNode.NodeType.DIALOGUE, "Node B"]}, [["A", "B"]], "TB", "flowchart TB\nA[Node A]-->B[Node B]\n"],
			[{"A": [MermaidNode.NodeType.STANDARD, ""], "B": [MermaidNode.NodeType.STANDARD, ""], "C": [MermaidNode.NodeType.STANDARD, ""]}, [["A", "B"], ["B", "C"]], "TB", "flowchart TB\nA-->B\nB-->C\n"],
		]) -> void:

	var m : MermaidParser = MermaidParser.new()
	m.orientation = orientation

	for node_id in nodes.keys():
		var node_properties = nodes[node_id]
		var node = MermaidNode.new(node_id, node_properties[1], node_properties[0])
		m.nodes[node_id] = node

	for link in links:
		m.nodes[link[0]].add_link(m.nodes[link[1]], "")

	var result = m.compose()
	assert_str(_clean_string(result)).is_equal(_clean_string(expected))
	_cleanup(m)


func test_from_markdown() -> void:
	var markdown_string = """
	# This is a Markdown text

	Here's a simple Mermaid chart:

	```mermaid
	graph TD
	A-->B
	B-->C
	```

	That's it!
	"""
	var mermaid = auto_free(MermaidParser.from_markdown(markdown_string))
	
	# Check if mermaid is not null
	assert_object(mermaid).is_not_null()
	if !mermaid:
		return
	
	# Check if the orientation and nodes size are correct
	assert_str(mermaid.orientation).is_equal("TD")
	assert_int(mermaid.nodes.size()).is_equal(3)

	# Check if nodes exist and are not null
	_assert_node_exists_and_is_not_null(mermaid, "A")
	_assert_node_exists_and_is_not_null(mermaid, "B")
	_assert_node_exists_and_is_not_null(mermaid, "C")

	# Check if node links count are correct
	_assert_node_links_count("A", 1, mermaid)
	_assert_node_links_count("B", 1, mermaid)
	_assert_node_links_count("C", 0, mermaid)

	# Call cleanup if you have it defined in your test script
	_cleanup(mermaid)


# Helper function to assert node links count
func _assert_node_links_count(node_name: String, expected_count: int, mermaid: MermaidParser) -> void:
	if mermaid.nodes.has(node_name):
		assert_int(mermaid.nodes[node_name].links.size()).is_equal(expected_count)
	else:
		assert_that(false).override_failure_message("Node '{node_name}' does not exist.").is_true()


func _assert_node_exists_and_is_not_null(mermaid, node_key: String) -> void:
	if mermaid.nodes.has(node_key):
		assert_object(mermaid.nodes[node_key]).is_not_null()
	else:
		assert_that(false).override_failure_message("Node '%s' does not exist." % node_key).is_true()


func _clean_string(s: String) -> String:
	return s.strip_edges().replace(" ", "").replace("\t", "")


func _cleanup(m: MermaidParser):
	for id in m.nodes.keys():
		for link in m.nodes[id].links:
			auto_free(link)
