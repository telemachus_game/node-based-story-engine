# GdUnit generated TestSuite
class_name MermaidParserParseTest
extends GdUnitTestSuite


var iterations := 100


func before():
	Print.silence_all()


# This tests if parse() can correctly parse flowchart orientation and nodes.
func test_parse_single_node() -> void:
	var mermaid = auto_free(MermaidParser.new())
	var flowchart_string = """
	flowchart TB
	A
	"""
	mermaid.parse(flowchart_string)
	assert_str(mermaid.orientation).is_equal("TB")
	assert_int(mermaid.nodes.size()).is_equal(1)
	_assert_node_exists_and_is_not_null(mermaid, "A")
	_cleanup(mermaid)


# This tests if parse() can correctly parse flowchart with multiple nodes and links.
func test_parse_multiple_nodes() -> void:
	var mermaid = auto_free(MermaidParser.new())
	var flowchart_string = """
	flowchart LR
	A-->B
	B-->C
	"""
	mermaid.parse(flowchart_string)
	assert_str(mermaid.orientation).is_equal("LR")
	assert_int(mermaid.nodes.size()).is_equal(3)
	_assert_node_exists_and_is_not_null(mermaid, "A")
	_assert_node_exists_and_is_not_null(mermaid, "B")
	_assert_node_exists_and_is_not_null(mermaid, "C")
	_assert_node_links_count("A", 1, mermaid)
	_assert_node_links_count("B", 1, mermaid)
	_assert_node_links_count("C", 0, mermaid)
	_cleanup(mermaid)


# This tests if parse() can handle comments and empty lines correctly.
func test_parse_with_comments_and_empty_lines() -> void:
	var mermaid = auto_free(MermaidParser.new())
	var flowchart_string = """
	%% This is a comment
	flowchart TD
	
	A-->B
	%% Another comment
	B-->C
	
	"""
	mermaid.parse(flowchart_string)
	assert_str(mermaid.orientation).is_equal("TD")
	assert_int(mermaid.nodes.size()).is_equal(3)
	_assert_node_exists_and_is_not_null(mermaid, "A")
	_assert_node_exists_and_is_not_null(mermaid, "B")
	_assert_node_exists_and_is_not_null(mermaid, "C")
	_assert_node_links_count("A", 1, mermaid)
	_assert_node_links_count("B", 1, mermaid)
	_assert_node_links_count("C", 0, mermaid)
	_cleanup(mermaid)


func test_parse_ignores_comments() -> void:
	var mermaid = auto_free(MermaidParser.new())
	var flowchart_string = """
	%% This is a comment
	flowchart TB
	A-->B
	"""
	mermaid.parse(flowchart_string)
	assert_int(mermaid.nodes.size()).is_equal(2)
	_assert_node_exists_and_is_not_null(mermaid, "A")
	_assert_node_exists_and_is_not_null(mermaid, "B")
	_cleanup(mermaid)


func test_parse_multiple_links_in_one_line() -> void:
	var mermaid = auto_free(MermaidParser.new())
	var flowchart_string = """
	flowchart TB
	A-->B-->C
	"""
	mermaid.parse(flowchart_string)
	assert_int(mermaid.nodes.size()).is_equal(3)
	_assert_node_links_count("A", 1, mermaid)
	_assert_node_links_count("B", 1, mermaid)
	_assert_node_links_count("C", 0, mermaid)
	_cleanup(mermaid)


func test_parse_multi_node_links_in_one_line() -> void:
	var mermaid = auto_free(MermaidParser.new())
	var flowchart_string = """
	flowchart TB
	A & B-->C & D
	"""
	mermaid.parse(flowchart_string)
	assert_int(mermaid.nodes.size()).is_equal(4)
	_assert_node_links_count("A", 2, mermaid)
	_assert_node_links_count("B", 2, mermaid)
	_assert_node_links_count("C", 0, mermaid)
	_assert_node_links_count("D", 0, mermaid)
	_cleanup(mermaid)


func test_parse_even_more_multi_node_links_in_one_line() -> void:
	var mermaid = auto_free(MermaidParser.new())
	var flowchart_string = """
	flowchart TB
	A & B & C-->D & E & F
	"""
	mermaid.parse(flowchart_string)
	assert_int(mermaid.nodes.size()).is_equal(6)
	_assert_node_links_count("A", 3, mermaid)
	_assert_node_links_count("B", 3, mermaid)
	_assert_node_links_count("C", 3, mermaid)
	_assert_node_links_count("D", 0, mermaid)
	_assert_node_links_count("E", 0, mermaid)
	_assert_node_links_count("F", 0, mermaid)
	_cleanup(mermaid)


func test_parse_empty_string() -> void:
	var mermaid = auto_free(MermaidParser.new())
	var flowchart_string = ""
	mermaid.parse(flowchart_string)
	assert_int(mermaid.nodes.size()).is_equal(0)
	_cleanup(mermaid)


func test_parse_creates_nonexistent_nodes() -> void:
	var mermaid = auto_free(MermaidParser.new())
	var flowchart_string = """
	flowchart TB
	A-->B
	"""
	mermaid.parse(flowchart_string)
	assert_int(mermaid.nodes.size()).is_equal(2)
	_assert_node_exists_and_is_not_null(mermaid, "A")
	_assert_node_exists_and_is_not_null(mermaid, "B")
	_cleanup(mermaid)


@warning_ignore("unused_parameter")
func test_parse_different_link_types(arrow_type: String, expected_link_type: MermaidLink.LinkType, test_parameters := [
	["-->", MermaidLink.LinkType.SOLID],
	["==>", MermaidLink.LinkType.THICK],
	["~~~", MermaidLink.LinkType.INVISIBLE],
	[".->", MermaidLink.LinkType.DOTTED]]) -> void:
	
	var mermaid = auto_free(MermaidParser.new())
	var flowchart_string = """
	flowchart TB
	A """ + arrow_type + """ B
	"""
	mermaid.parse(flowchart_string)
	assert_int(mermaid.nodes.size()).is_equal(2)
	_assert_node_exists_and_is_not_null(mermaid, "A")
	_assert_node_exists_and_is_not_null(mermaid, "B")
	if mermaid.nodes.has("A"):
		if mermaid.nodes["A"].links.size() > 0:
			assert_that(mermaid.nodes["A"].links[0].type).is_equal(expected_link_type)
		else:
			assert_that(false).override_failure_message("Node 'A' does not contain links!.").is_true()
	else:
		assert_that(false).override_failure_message("Node 'A' does not exist.").is_true()
	_cleanup(mermaid)


@warning_ignore("unused_parameter")
func test_parse_different_arrow_types(arrow_type: String, expected_arrow_type: MermaidLink.ArrowType, test_parameters := [
	["-->", MermaidLink.ArrowType.ARROW],
	["<-->", MermaidLink.ArrowType.ARROW],
	["--o", MermaidLink.ArrowType.SOLID],
	["o--o", MermaidLink.ArrowType.SOLID],
	["--x", MermaidLink.ArrowType.CROSS],
	["x--x", MermaidLink.ArrowType.CROSS]]) -> void:
	
	var mermaid = auto_free(MermaidParser.new())
	var flowchart_string = """
	flowchart TB
	A """ + arrow_type + """ B
	"""
	mermaid.parse(flowchart_string)
	assert_int(mermaid.nodes.size()).is_equal(2)
	_assert_node_exists_and_is_not_null(mermaid, "A")
	_assert_node_exists_and_is_not_null(mermaid, "B")
	if mermaid.nodes.has("A"):
		if mermaid.nodes["A"].links.size() > 0:
			assert_that(mermaid.nodes["A"].links[0].arrow).is_equal(expected_arrow_type)
		else:
			assert_that(false).override_failure_message("Node 'A' does not contain links!.").is_true()
	else:
		assert_that(false).override_failure_message("Node 'A' does not exist.").is_true()
	_cleanup(mermaid)


@warning_ignore("unused_parameter")
func test_parse_different_link_directions(arrow_type: String, expected_link_direction: MermaidLink.LinkDirection, test_parameters := [
	["-->", MermaidLink.LinkDirection.UNIDIRECTIONAL],
	["--o", MermaidLink.LinkDirection.UNIDIRECTIONAL],
	["--x", MermaidLink.LinkDirection.UNIDIRECTIONAL],
	["==>", MermaidLink.LinkDirection.UNIDIRECTIONAL],
	["==o", MermaidLink.LinkDirection.UNIDIRECTIONAL],
	["==x", MermaidLink.LinkDirection.UNIDIRECTIONAL],
	[".->", MermaidLink.LinkDirection.UNIDIRECTIONAL],
	[".-o", MermaidLink.LinkDirection.UNIDIRECTIONAL],
	[".-x", MermaidLink.LinkDirection.UNIDIRECTIONAL],
	["<-->", MermaidLink.LinkDirection.BIDIRECTIONAL],
	["o--o", MermaidLink.LinkDirection.BIDIRECTIONAL],
	["x--x", MermaidLink.LinkDirection.BIDIRECTIONAL],
	["<==>", MermaidLink.LinkDirection.BIDIRECTIONAL],
	["o==o", MermaidLink.LinkDirection.BIDIRECTIONAL],
	["x==x", MermaidLink.LinkDirection.BIDIRECTIONAL],
	["<.->", MermaidLink.LinkDirection.BIDIRECTIONAL],
	["o.-o", MermaidLink.LinkDirection.BIDIRECTIONAL],
	["x.-x", MermaidLink.LinkDirection.BIDIRECTIONAL],
	["---", MermaidLink.LinkDirection.OPEN]]) -> void:
	
	var mermaid = auto_free(MermaidParser.new())
	var flowchart_string = """
	flowchart TB
	A """ + arrow_type + """ B
	"""
	mermaid.parse(flowchart_string)
	assert_int(mermaid.nodes.size()).is_equal(2)
	_assert_node_exists_and_is_not_null(mermaid, "A")
	_assert_node_exists_and_is_not_null(mermaid, "B")
	if mermaid.nodes.has("A"):
		if mermaid.nodes["A"].links.size() > 0:
			assert_that(mermaid.nodes["A"].links[0].direction).is_equal(expected_link_direction)
		else:
			assert_that(false).override_failure_message("Node 'A' does not contain links!.").is_true()
	else:
		assert_that(false).override_failure_message("Node 'A' does not exist.").is_true()
	_cleanup(mermaid)


@warning_ignore("unused_parameter")
func test_parse_different_link_lengths(arrow_type: String, expected_link_length: int, test_parameters := [
	["-->", 1],
	["--->", 2],
	["---->", 3],
	["-- Text on Link -->", 1],
	["---", 1],
	["==>", 1],
	["===>", 2],
	["====>", 3],
	["== Text on Link ==>", 1],
	["===", 1],
	["-.->", 1],
	["-..->", 2],
	["-...->", 3],
	["-. Text on Link .->", 1],
	[".->", 1],
	["..->", 2],
	["...->", 3],
	["-. Text on Link .->", 1],
	["..-", 1]]) -> void:
	
	var mermaid = auto_free(MermaidParser.new())
	var flowchart_string = """
	flowchart TB
	A """ + arrow_type + """ B
	"""
	mermaid.parse(flowchart_string)
	assert_int(mermaid.nodes.size()).is_equal(2)
	_assert_node_exists_and_is_not_null(mermaid, "A")
	_assert_node_exists_and_is_not_null(mermaid, "B")
	if mermaid.nodes.has("A"):
		if mermaid.nodes["A"].links.size() > 0:
			assert_that(mermaid.nodes["A"].links[0].length).is_equal(expected_link_length)
		else:
			assert_that(false).override_failure_message("Node 'A' does not contain links!.").is_true()
	else:
		assert_that(false).override_failure_message("Node 'A' does not exist.").is_true()
	_cleanup(mermaid)


func test_parse_ignores_extra_spaces() -> void:
	var mermaid = auto_free(MermaidParser.new())
	var flowchart_string = """
	flowchart TB
	A  -->  B
	"""
	mermaid.parse(flowchart_string)
	assert_int(mermaid.nodes.size()).is_equal(2)
	_assert_node_exists_and_is_not_null(mermaid, "A")
	_assert_node_exists_and_is_not_null(mermaid, "B")
	_cleanup(mermaid)


@warning_ignore("unused_parameter")
func test_parse_different_node_types(node_text: String, expected_node_text: String, expected_node_type, expected_node_class, test_parameters := [
	["", "", MermaidNode.NodeType.STANDARD, MermaidNode],
	["[This is a dialogue node]", "This is a dialogue node", MermaidNode.NodeType.DIALOGUE, MermaidDialogueNode],
	["[[This is a box node]]", "This is a box node", MermaidNode.NodeType.SUBGRAPH, MermaidSubgraphNode],
	["{This is a rhombus node}", "This is a rhombus node", MermaidNode.NodeType.COMMAND, MermaidCommandNode],
	["(This is a node with rounded edges)", "This is a node with rounded edges", MermaidNode.NodeType.ROUNDED, MermaidUndefinedNode],
	["([This is a stadium node])", "This is a stadium node", MermaidNode.NodeType.STADIUM, MermaidUndefinedNode],
	["(((This is a double circle node)))", "This is a double circle node", MermaidNode.NodeType.DOUBLECIRCLE, MermaidUndefinedNode],
	["[(This is a cylindrical node)]", "This is a cylindrical node", MermaidNode.NodeType.CYLINDRICAL, MermaidUndefinedNode],
	["((This is a circular node))", "This is a circular node", MermaidNode.NodeType.CIRCULAR, MermaidUndefinedNode],
	["{{This is a hexagon node}}", "This is a hexagon node", MermaidNode.NodeType.HEXAGON, MermaidUndefinedNode],
	["[\\This is a parallelogram node\\]", "This is a parallelogram node", MermaidNode.NodeType.PARALLELOGRAM, MermaidUndefinedNode],
	["[/This is also a parallelogram node/]", "This is also a parallelogram node", MermaidNode.NodeType.PARALLELOGRAM2, MermaidUndefinedNode],
	["[/This is a trapezoid node\\]", "This is a trapezoid node", MermaidNode.NodeType.TRAPEZOID, MermaidUndefinedNode],
	["[\\This is also a trapezoid node/]", "This is also a trapezoid node", MermaidNode.NodeType.TRAPEZOID2, MermaidUndefinedNode],
	[">This is a flag shaped node]", "This is a flag shaped node", MermaidNode.NodeType.FLAG, MermaidUndefinedNode]]) -> void:
	
	var mermaid = auto_free(MermaidParser.new())
	var flowchart_string = "flowchart TB\nA" + node_text
	mermaid.parse(flowchart_string)
	assert_int(mermaid.nodes.size()).is_equal(1)
	if mermaid.nodes.has("A"):
		assert_str(mermaid.nodes["A"].raw_text).is_equal(expected_node_text)
		assert_str(MermaidNode.NodeType.keys()[mermaid.nodes["A"].type]).is_equal(MermaidNode.NodeType.keys()[expected_node_type])
		assert_object(mermaid.nodes["A"]).is_instanceof(expected_node_class)
	else:
		assert_that(false).override_failure_message("Node 'A' does not exist.").is_true()
	_cleanup(mermaid)


# This tests if parse() can correctly parse flowchart orientation and nodes.
@warning_ignore("unused_parameter")
func test_parse_node_types_redefinition(flowchart_string: String, node_type, test_parameters := [
		["flowchart TD\nA", MermaidNode],
		["flowchart TD\nA\nA[dialogue]", MermaidDialogueNode],
		["flowchart TD\nA\nA[[box]]", MermaidSubgraphNode],
		["flowchart TD\nA\nA{rhombus}", MermaidCommandNode],
		["flowchart TD\nA\nA(rounded)", MermaidUndefinedNode],
		["flowchart TD\nA\nA([stadium])", MermaidUndefinedNode],
		["flowchart TD\nA\nA(((double circle)))", MermaidUndefinedNode],
		["flowchart TD\nA\nA[(cylindrical)]", MermaidUndefinedNode],
		["flowchart TD\nA\nA((circular))", MermaidUndefinedNode],
		["flowchart TD\nA\nA{{hexagon}}", MermaidUndefinedNode],
		["flowchart TD\nA\nA[\\parallelogram\\]", MermaidUndefinedNode],
		["flowchart TD\nA\nA[/parallelogram/]", MermaidUndefinedNode],
		["flowchart TD\nA\nA[/trapezoid\\]", MermaidUndefinedNode],
		["flowchart TD\nA\nA[\\trapezoid/]", MermaidUndefinedNode],
		["flowchart TD\nA\nA>flag]", MermaidUndefinedNode]
	]) -> void:
	var mermaid: MermaidParser = auto_free(MermaidParser.new(flowchart_string))
	assert_int(mermaid.nodes.size()).is_equal(1)
	var a = mermaid.nodes["A"]
	assert_object(a).is_instanceof(node_type)
	_cleanup(mermaid)


@warning_ignore("unused_parameter")
func test_parse_links_with_text(arrow_type: String, expected_link_note: String, test_parameters := [
	["-->", ""],
	[" --> ", ""],
	["---", ""],
	[" --- ", ""],
	["--Text on Link---", "Text on Link"],
	[" -- Text on Link --- ", "Text on Link"],
	["---|Text on Link|", "Text on Link"],
	[" --- |Text on Link| ", "Text on Link"],
	["-->|Text on Link|", "Text on Link"],
	[" --> |Text on Link| ", "Text on Link"],
	["--Text on Link-->", "Text on Link"],
	[" -- Text on Link --> ", "Text on Link"],
	["-.->", ""],
	[" -.-> ", ""],
	["-.Text on Link.->", "Text on Link"],
	[" -. Text on Link .-> ", "Text on Link"],
	["-.->|Text on Link|", "Text on Link"],
	[" -.-> |Text on Link| ", "Text on Link"],
	["==>", ""],
	[" ==> ", ""],
	["==Text on Link==>", "Text on Link"],
	[" == Text on Link ==> ", "Text on Link"],
	["===>|Text on Link|", "Text on Link"],
	[" ===> |Text on Link| ", "Text on Link"],
	["~~~", ""],
	[" ~~~ ", ""]]) -> void:
	
	var mermaid = auto_free(MermaidParser.new())
	var flowchart_string = """
	flowchart TB
	A """ + arrow_type + """ B
	"""
	mermaid.parse(flowchart_string)
	assert_int(mermaid.nodes.size()).is_equal(2)
	_assert_node_exists_and_is_not_null(mermaid, "A")
	_assert_node_exists_and_is_not_null(mermaid, "B")
	if mermaid.nodes.has("A"):
		if mermaid.nodes["A"].links.size() > 0:
			assert_str(mermaid.nodes["A"].links[0].note).is_equal(expected_link_note)
		else:
			assert_that(false).override_failure_message("Node 'A' does not contain links!.").is_true()
	else:
		assert_that(false).override_failure_message("Node 'A' does not exist.").is_true()
	_cleanup(mermaid)


# This tests if parse() can correctly parse flowchart orientation and nodes.
@warning_ignore("unused_parameter")
func test_parse_expected_warnings(flowchart_string: String, warning_count, test_parameters := [
		["flowchart TD\nA\n%% Basic Node"                                                           , 0],
		["flowchart TD\nA[Text]\n%% Node with text"                                                 , 0],
		["flowchart TD\nA\nA[Dialogue]\n%% Allowed Redefinition of node type and text"              , 0],
		["flowchart TD\nA\nA[Text1]\nA[Text2]\n%% Redefinition of node text"                        , 1],
		["flowchart TD\nA\nA[Text]\nA{Text}\n%% Redefinition of node type"                          , 1],
		["flowchart TD\nA[[Subgraph]]\nA[Dialogue]\n%% Redefinition of node type and text"          , 2],
		["flowchart TD\nA-->A[Dialogue]\n%% One line allowed redefinition"                          , 0],
		["flowchart TD\nA{Diamond}-->A[Dialogue]\n%% Invalid redefinition of type and text"         , 2],
		["flowchart TD\nA-->A[Dialogue]-->A\n%% Valid redefinition of type"                         , 0],
		["flowchart TD\nA-->A[Dialogue]-->A{Diamond}\n%% Invalid redefinition of type and text"     , 2],
		["flowchart TD\nA[Dialogue]-->A[Dialogue]\n%% Duplicate definition of type and text"        , 0],
	]) -> void:
	Print.clear_warnings()
	var mermaid: MermaidParser = auto_free(MermaidParser.new(flowchart_string))
	assert_int(Print.warning_count).is_equal(warning_count)
	_cleanup(mermaid)


# This tests if parse() can correctly parse flowchart orientation and nodes.
@warning_ignore("unused_parameter")
func test_parse_expected_errors(flowchart_string: String, error_count, test_parameters := [
		["flowchart TD\nA[Dialogue]-->sg[[graph.md]]-->B\n     %% Subgraph cannot find file to load.  "             , 1],
		["flowchart TD\na-->b\nsubgraph sg\nb-->c\n            %% Subgraph does not close"                          , 2],
		["flowchart TD\na-->b\nend\n                           %% Reserved keyword 'end' used outside subgraph"     , 1],
	]) -> void:
	Print.clear_errors()
	var mermaid: MermaidParser = auto_free(MermaidParser.new(flowchart_string))
	assert_int(Print.error_count).is_equal(error_count)
	_cleanup(mermaid)


@warning_ignore("unused_parameter")
func test_parse_start_end_node(flowchart_string: String, expected_start_node: String, expected_end_nodes: Array, test_parameters := [
		["flowchart TD\nA-->B-->C & D\n                %% No command nodes.             ", "A", []],
		["flowchart TD\nA-->B-->exit{C}\n              %% Exit node defined.            ", "A", ["exit"]],
		["flowchart TD\nA-->start{B}-->exit{C}\n       %% Start and exit nodes defined. ", "start", ["exit"]],
		["flowchart TD\nA-->start[B]-->exit[C]\n       %% Nodes named start and exit.   ", "A", []],
		["flowchart TD\nA-->start-->B-->C\nstart{A}\n  %% Start node redefined.         ", "start", []],
		["flowchart TD\nA-->start-->B-->C\nstart[A]\n  %% Node named start redefined.   ", "A", []],
		["flowchart TD\nexit{C}\nstart{A}              %% Disconnected start and exit.  ", "start", ["exit"]],
		["flowchart TD\nA-->exit_0{B}\nA-->exit_1{C}\n %% Two exits                     ", "A", ["exit_0", "exit_1"]],
		["flowchart TD\nA-->B-->exit_0{C}\nstart{D}-->exit_1{C} %% More exits.          ", "start", ["exit_0", "exit_1"]],
	]) -> void:
	var mermaid: MermaidParser = auto_free(MermaidParser.new(flowchart_string))
	
	if mermaid.start_node:
		assert_str(mermaid.start_node.id).is_equal(expected_start_node)
	else:
		assert_object(mermaid.start_node).is_not_null()
	
	var end_nodes = []
	for node in mermaid.exit_nodes:
		end_nodes.append(node.id)
	
	assert_array(end_nodes).contains_exactly(expected_end_nodes)
	assert_int(mermaid.exit_nodes.size()).is_equal(expected_end_nodes.size())
	_cleanup(mermaid)


@warning_ignore("unused_parameter")
func test_parse_fuzz():
	seed(0)
	for _i in iterations:
		var flow_gen := FlowchartGenerator.new()
		flow_gen.generate_flowchart(3)
		var mermaid = MermaidParser.new(flow_gen.flowchart)
		flow_gen.assert_equals(mermaid)
		assert_str(flow_gen.flowchart).is_equal(mermaid.compose())
		flow_gen.queue_free()


@warning_ignore("unused_parameter")
func test_parse_fuzz_custom_chart():
	seed(0)
	for _i in iterations:
		var flow_gen := FlowchartGenerator.new()
		flow_gen.begin_chart(flow_gen.random_string(randi_range(1, 10)), flow_gen.random_string(randi_range(1, 10)))
		for i in 4:
			flow_gen.chain_node(flow_gen.random_string(randi_range(1, 10)), flow_gen.random_string(randi_range(1, 10)))
		
		var mermaid = MermaidParser.new(flow_gen.flowchart)
		flow_gen.assert_equals(mermaid)
		assert_str(flow_gen.flowchart).is_equal(mermaid.compose())
		flow_gen.queue_free()


# Helper function to assert node links count
func _assert_node_links_count(node_name: String, expected_count: int, mermaid: MermaidParser) -> void:
	if mermaid.nodes.has(node_name):
		assert_int(mermaid.nodes[node_name].links.size()).is_equal(expected_count)
	else:
		assert_that(false).override_failure_message("Node '{node_name}' does not exist.").is_true()


func _assert_node_exists_and_is_not_null(mermaid, node_key: String) -> void:
	if mermaid.nodes.has(node_key):
		assert_object(mermaid.nodes[node_key]).is_not_null()
	else:
		assert_that(false).override_failure_message("Node '%s' does not exist." % node_key).is_true()


func _cleanup(m: MermaidParser):
	for id in m.nodes.keys():
		for link in m.nodes[id].links:
			auto_free(link)
