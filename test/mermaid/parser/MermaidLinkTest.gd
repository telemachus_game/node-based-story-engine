# GdUnit generated TestSuite
class_name MermaidLinkTest
extends GdUnitTestSuite
@warning_ignore('unused_parameter')
@warning_ignore('return_value_discarded')

# TestSuite generated from
const __source = 'res://mermaid/MermaidLink.gd'


func test_split_line_regular_case():
	var result = MermaidLink.split_line("A-->B")
	assert_array(result).has_size(2)
	assert_array(result[0]).is_equal(["A", "-->"])
	assert_array(result[1]).is_equal(["B", ""])


func test_split_line_whitespace():
	var result = MermaidLink.split_line("  A  -->  B  ")
	assert_array(result).has_size(2)
	assert_array(result[0]).is_equal(["A", "-->"])
	assert_array(result[1]).is_equal(["B", ""])


func test_split_line_multiple_arrows():
	var result = MermaidLink.split_line("A-->B-->C")
	assert_array(result).has_size(3)
	assert_array(result[0]).is_equal(["A", "-->"])
	assert_array(result[1]).is_equal(["B", "-->"])
	assert_array(result[2]).is_equal(["C", ""])


func test_split_line_looping_arrows():
	var result = MermaidLink.split_line("A-->B-->C-->A")
	assert_array(result).has_size(4)
	assert_array(result[0]).is_equal(["A", "-->"])
	assert_array(result[1]).is_equal(["B", "-->"])
	assert_array(result[2]).is_equal(["C", "-->"])
	assert_array(result[3]).is_equal(["A", ""])


func test_split_line_single_node():
	var result = MermaidLink.split_line("A")
	assert_array(result).has_size(1)
	assert_array(result[0]).is_equal(["A", ""])


func test_split_line_node_with_spaces():
	var result = MermaidLink.split_line("A B C-->D E F")
	assert_array(result).has_size(2)
	assert_array(result[0]).is_equal(["A B C", "-->"])
	assert_array(result[1]).is_equal(["D E F", ""])
