# GdUnit generated TestSuite
class_name MermaidParserParseComposeTest
extends GdUnitTestSuite


func before():
	Print.silence_all()


## Tests every major Mermaid feature that I could think of. Reads, then writes the Mermaid string.
## Fails if the two graphs are not identical.
@warning_ignore("unused_parameter")
func test_parse_compose_basic(chart: String, test_parameters := [
	["flowchart TB\nA"],
	["flowchart TD\nA"],
	["flowchart BT\nA"],
	["flowchart LR\nA"],
	["flowchart RL\nA"],
	["flowchart TB\nA-->B"],
	["flowchart TB\nA-->B\nB-->C"],
	["flowchart TB\nA-->B\nB-->C\nC-->D"],
	["flowchart TB\nA[Node A]"],
	["flowchart TB\nA[Node A]-->B[Node B]"],
	["flowchart TB\nA[Node A]-->B[Node B]\nB-->C[Node C]"],
	["flowchart TB\nA[Node A]-->B[Node B]\nA-->C[Node C]\nB-->C"]
]) -> void:
	_test_parse_compose(chart)


## Tests every single node type. Reads, then writes the Mermaid string.
## Fails if the two graphs are not identical.
@warning_ignore("unused_parameter")
func test_parse_compose_node_types(chart: String, test_parameters := [
	["flowchart TD\nid[This is a standard node]"],
	["flowchart TD\nid(This is a node with rounded edges)"],
	["flowchart TD\nid([This is a stadium node])"],
	["flowchart TD\nid[[\"graph TD\n%% This is a subgraph node\nexit {x}\"]]-->B"],
	["flowchart TD\nid[(This is a cylindrical node)]"],
	["flowchart TD\nid((This is a circular node))"],
	["flowchart TD\nid{This is a rhombus node}"],
	["flowchart TD\nid{{This is a hexagon node}}"],
	["flowchart TD\nid[/This is a parallelogram node/]"],
	["flowchart TD\nid[\\This is also a parallelogram node\\]"],
	["flowchart TD\nid[/This is a trapezoid node\\]"],
	["flowchart TD\nid[\\This is also a trapezoid node/]"],
	["flowchart TD\nid(((This is a double circle node)))"],
	["flowchart TD\nid>This is a flag shaped node]"]
]) -> void:
	_test_parse_compose(chart)


## Tests every single link type that does not undergo transformation.
## Reads, then writes the Mermaid string.
## Fails if the two graphs are not identical.
@warning_ignore("unused_parameter")
func test_parse_compose_link_types(chart: String, test_parameters := [
	["flowchart TD\nA-->B"],
	["flowchart TD\nA --> B"],
	["flowchart TD\nA---B"],
	["flowchart TD\nA --- B"],
	["flowchart TD\nA--Text on Link---B"],
	["flowchart TD\nA -- Text on Link --- B"],
	["flowchart TD\nA--Text on Link-->B"],
	["flowchart TD\nA -- Text on Link --> B"],
	["flowchart TD\nA.->B"],
	["flowchart TD\nA .-> B"],
	["flowchart TD\nA-.Text on Link.->B"],
	["flowchart TD\nA -. Text on Link .-> B"],
	["flowchart TD\nA==>B"],
	["flowchart TD\nA ==> B"],
	["flowchart TD\nA==Text on Link==>B"],
	["flowchart TD\nA == Text on Link ==> B"],
	["flowchart TD\nA~~~B"],
	["flowchart TD\nA ~~~ B"],
]) -> void:
	_test_parse_compose(chart)


## Tests every single link type. Reads, then writes the Mermaid string.
## Fails if the two graphs are not identical.
@warning_ignore("unused_parameter")
func test_parse_compose_transformed_link_types(initial_chart: String, expected_chart: String, test_parameters := [
	["flowchart TD\nA---|Text on Link|B", "flowchart TD\nA-- Text on Link --- B"],
	["flowchart TD\nA --- |Text on Link| B", "flowchart TD\nA -- Text on Link --- B"],
	["flowchart TD\nA-->|Text on Link|B", "flowchart TD\nA-- Text on Link -->B"],
	["flowchart TD\nA --> |Text on Link| B", "flowchart TD\nA -- Text on Link --> B"],
	["flowchart TD\nA-.->B", "flowchart TD\nA.->B"],
	["flowchart TD\nA -.-> B", "flowchart TD\nA .-> B"],
	["flowchart TD\nA-.->|Text on Link|B", "flowchart TD\nA -. Text on Link .-> B"],
	["flowchart TD\nA -.-> |Text on Link| B", "flowchart TD\nA -. Text on Link .-> B"],
	["flowchart TD\nA===>|Text on Link|B", "flowchart TD\nA== Text on Link ===> B"],
	["flowchart TD\nA ===> |Text on Link| B", "flowchart TD\nA == Text on Link ===> B"],
	["flowchart TD\nA-->B-->C-->A", "flowchart TD\nA-->B\nB-->C\nC-->A"],
	["flowchart TD\nA\nA[The story starts here]\nA --> B", "flowchart TD\nA[The story starts here]-->B"],
	["flowchart TD\nA[This text gets overwritten]\nA[The story starts here]\nA --> B", "flowchart TD\nA[The story starts here]-->B"],
	["flowchart TD\nA[This text gets overwritten]\nA{The story starts here}\nA --> B", "flowchart TD\nA{The story starts here}-->B"],
]) -> void:
	_test_parse_compose_transformed(initial_chart, expected_chart)


## Tests inline subgraphs. Reads, then writes the Mermaid string.
@warning_ignore("unused_parameter")
func test_parse_compose_mermaid_subgraphs(initial_chart: String, expected_chart: String, expected_errors: int, test_parameters := [
	# Empty subgraph
	["flowchart TB\nA\nsubgraph sub1\nend\n","flowchart TB\nA\nsubgraph sub1\nend", 1],
	# Subgraph with disconnected node
	["flowchart TB\nA\nsubgraph sub1\nC\nend\n","flowchart TB\nA\nsubgraph sub1\nC\nend", 0],
	# Subgraph with nodes connected directly
	["flowchart TB\nA\nsubgraph sub1\nC\nend\nA-->C\n","flowchart TB\nA-->C\nsubgraph sub1\nC\nend", 0],
	# Subgraph connected directly with hanging node.
	["flowchart TB\nA\nsubgraph sub1\nC\nend\nA-->sub1\nC-->D","flowchart TB\nA-->sub1\nsubgraph sub1\nC\nend\nC-->D", 0],
	# Subgraph connected directly with exit node.
	["flowchart TB\nA\nsubgraph sub1\nC\nend\n","flowchart TB\nA\nsubgraph sub1\nC\nend", 0],
	# Empty Subgraph declared before defined.
	["flowchart TB\nA-->sub1\nsubgraph sub1\nend\n","flowchart TB\nA-->sub1\nsubgraph sub1\nend", 1],
	# Subgraph declared before defined with direct exit.
	["flowchart TB\nA-->sub1\nsubgraph sub1\nC\nend\n","flowchart TB\nA-->sub1\nsubgraph sub1\nC\nend", 0],
	# Subgraph declared before defined with exit node.
	["flowchart TB\nA-->sub1\nsubgraph sub1\nC-->exit{leave subgraph}\nend","flowchart TB\nA-->sub1\nsubgraph sub1\nC-->exit{leave subgraph}\nend", 0],
	# Node defined outside but included inside a subgraph
	["flowchart TB\na-->b\nsubgraph sub1\nb\nend\nb-->c\n","flowchart TB\na-->b\nb-->c\nsubgraph sub1\nb\nend\n", 0],
	# Multiple nodes defined inside a subgraph
	["flowchart TB\na-->sub1\nsubgraph sub1\nb-->c\nend\n","flowchart TB\na-->sub1\nsubgraph sub1\nb-->c\nend\n", 0],
	# Multiple nodes defined outside but included inside a subgraph
	["flowchart TB\na-->b\nb-->c\nsubgraph sub1\nb\nc\nend\n","flowchart TB\na-->b\nb-->c\nsubgraph sub1\nb\nc\nend\n", 0],
	# Node defined both inside and outside a subgraph
	["flowchart TB\na-->b\nsubgraph sub1\nb-->c\nend\n","flowchart TB\na-->b\nsubgraph sub1\nb-->c\nend\n", 0],
	# Node connections between subgraphs
	["flowchart TB\na-->sub1\nsubgraph sub1\nb\nend\nsubgraph sub2\nc\nend\nb-->c\n","flowchart TB\na-->sub1\nsubgraph sub1\nb\nend\nsubgraph sub2\nc\nend\nb-->c\n", 0],
	# Nested subgraphs.
	["flowchart TB\na-->sub1\nsubgraph sub1\nb\nsubgraph sub2\nc\nend\nend\nb-->c\n","flowchart TB\na-->sub1\nsubgraph sub1\nb\nsubgraph sub2\nc\nend\nend\nb-->c\n", 0],
	# Subgraph with nodes connected outside of graph.
	["flowchart TB\nA\nsubgraph sub1\nC\nend\nA-->C\nC-->D","flowchart TB\nA-->C\nsubgraph sub1\nC\nend\nC-->D", 0],
]) -> void:
	# Skipping error check because subgraphs will complain if empty.
	_test_parse_compose_transformed(initial_chart, expected_chart, expected_errors)


## Tests inline subgraphs. Reads, then writes the Mermaid string.
@warning_ignore("unused_parameter")
func test_parse_compose_subgraph_nodes(chart: String, expected_errors: int, test_parameters := [
	# Subgraph with one exit node.
	["flowchart TD\nsg[[\"graph TD\na-->exit{x}\"]]-->b", 0],
	# Subgraph with zero exit nodes.
	["flowchart TD\nsg[[\"graph TD\na\"]]", 0],
	# Subgraph with one exit node but zero links.
	["flowchart TD\nsg[[\"graph TD\na-->exit{x}\"]]", 1],
	# Subgraph with one link but zero exit nodes.
	["flowchart TD\nsg[[\"graph TD\na\"]]-->b", 1],
	# Subgraph with two exit nodes.
	["flowchart TD\nsg[[\"graph TD\na-->exit_0{x}\na-->exit_1{y}\"]]-->b\nsg-->c", 0],
	# Subgraph with two exit nodes but one link.
	["flowchart TD\nsg[[\"graph TD\na-->exit_0{x}\na-->exit_1{y}\"]]-->b", 1],
	# Subgraph with two exit nodes but zero links.
	["flowchart TD\nsg[[\"graph TD\na-->exit_0{x}\na-->exit_1{y}\"]]", 1],
	# Subgraph with one exit nodes but two links.
	["flowchart TD\nsg[[\"graph TD\na-->exit{x}\"]]-->b\nsg-->c", 1],
	# Subgraph with zero exit nodes but two links.
	["flowchart TD\nsg[[\"graph TD\na\"]]-->b\nid-->c", 1],
]) -> void:
	# Skipping error check because subgraphs will complain if empty.
	_test_parse_compose(chart, expected_errors)


## Tests every Mermaid graph from the documentation. Reads, then writes the Mermaid string.
## Fails if the two graphs are not identical.
@warning_ignore("unused_parameter")
func test_parse_compose_all(initial_chart: String, expected_chart: String, test_parameters := [
	["flowchart LR\nid", "flowchart LR\nid"],
	["flowchart LR\nid1[This is the text in the box]", "flowchart LR\nid1[This is the text in the box]"],
	["flowchart LR\nid[\"This ❤ Unicode\"]", "flowchart LR\nid[\"This ❤ Unicode\"]"],
	["flowchart LR\nmarkdown[\"`This **is** _Markdown_`\"]\nnewLines[\"`Line1\nLine 2\nLine 3`\"]\nmarkdown --> newLines", "flowchart LR\nmarkdown[\"`This **is** _Markdown_`\"]-->newLines[\"`Line1\nLine 2\nLine 3`\"]"],
	["flowchart TD\nStart --> Stop", "flowchart TD\nStart --> Stop"],
	["flowchart LR\nStart --> Stop", "flowchart LR\nStart --> Stop"],
	["flowchart LR\nid1(This is the text in the box)", "flowchart LR\nid1(This is the text in the box)"],
	["flowchart LR\nid1([This is the text in the box])", "flowchart LR\nid1([This is the text in the box])"],
	["flowchart LR\nid1[[\"graph TD\nexit {x}\"]]-->A", "flowchart LR\nid1[[\"graph TD\nexit {x}\"]]-->A"],
	["flowchart LR\nid1[(Database)]", "flowchart LR\nid1[(Database)]"],
	["flowchart LR\nid1((This is the text in the circle))", "flowchart LR\nid1((This is the text in the circle))"],
	["flowchart LR\nid1{This is the text in the box}", "flowchart LR\nid1{This is the text in the box}"],
	["flowchart LR\nid1{{This is the text in the box}}", "flowchart LR\nid1{{This is the text in the box}}"],
	["flowchart TD\nid1[/This is the text in the box/]", "flowchart TD\nid1[/This is the text in the box/]"],
	["flowchart TD\nid1[\\This is the text in the box\\]", "flowchart TD\nid1[\\This is the text in the box\\]"],
	["flowchart TD\nA[/Christmas\\]", "flowchart TD\nA[/Christmas\\]"],
	["flowchart TD\nB[\\Go shopping\\]", "flowchart TD\nB[\\Go shopping\\]"],
	["flowchart TD\nid1(((This is the text in the circle)))", "flowchart TD\nid1(((This is the text in the circle)))"],
	["flowchart LR\nA-->B", "flowchart LR\nA-->B"],
	["flowchart LR\nA --- B", "flowchart LR\nA --- B"],
	["flowchart LR\nA-- This is the text! ---B", "flowchart LR\nA-- This is the text! ---B"],
	["flowchart LR\nA---|This is the text|B", "flowchart LR\nA--This is the text---B"],
	["flowchart LR\nA-->|text|B", "flowchart LR\nA--text-->B"],
	["flowchart LR\nA-- text -->B", "flowchart LR\nA-- text -->B"],
	["flowchart LR\nA-.->B;", "flowchart LR\nA.->B;"],
	["flowchart LR\nA-. text .-> B", "flowchart LR\nA-. text .-> B"],
	["flowchart LR\nA ==> B", "flowchart LR\nA ==> B"],
	["flowchart LR\nA == text ==> B", "flowchart LR\nA == text ==> B"],
	["flowchart LR\nA ~~~ B", "flowchart LR\nA ~~~ B"],
	["flowchart LR\nA -- text --> B -- text2 --> C", "flowchart LR\nA -- text --> B\nB -- text2 --> C"],
	["flowchart LR\na --> b & c--> d", "flowchart LR\na --> b\na --> c\nb --> d\nc--> d"],
	["flowchart TB\nA & B--> C & D", "flowchart TB\nA-->C\nA-->D\nB-->C\nB-->D"],
	["flowchart TB\nA --> C\nA --> D\nB --> C\nB --> D", "flowchart TB\nA --> C\nA --> D\nB --> C\nB --> D"],
	["flowchart LR\nA --o B\nB --x C", "flowchart LR\nA --o B\nB --x C"],
	["flowchart LR\nA o--o B\nB <--> C\nC x--x D", "flowchart LR\nA o--o B\nB <--> C\nC x--x D"],
	["flowchart TD\nA[Start] --> B{Is it?}\nB -->|Yes| C[OK]\nC --> D[Rethink]\nD --> B\nB ---->|No| E[End]", "flowchart TD\nA[Start] --> B{Is it?}\nB -- Yes --> C[OK]\nB -- No ----> E[End]\nC --> D[Rethink]\nD --> B"],
	["flowchart TD\nA[Start] --> B{Is it?}\nB -- Yes --> C[OK]\nC --> D[Rethink]\nD --> B\nB -- No ----> E[End]", "flowchart TD\nA[Start] --> B{Is it?}\nB -- Yes --> C[OK]\nB -- No ----> E[End]\nC --> D[Rethink]\nD --> B"],
	["flowchart LR\nid1[\"This is the (text) in the box\"]", "flowchart LR\nid1[\"This is the (text) in the box\"]"],
	["flowchart LR\nid1[\"This is the (text) in the box\"]", "flowchart LR\nid1[\"This is the (text) in the box\"]"],
	["flowchart LR\nA[\"A double quote:#quot;\"] -->B[\"A dec char:#9829;\"]", "flowchart LR\nA[\"A double quote:#quot;\"] -->B[\"A dec char:#9829;\"]"],
	["flowchart LR\nA[\"Werds\"] -->B[\"More Werds\"]", "flowchart LR\nA[\"Werds\"] -->B[\"More Werds\"]"],
	["flowchart LR\nc1-->a2\nsubgraph one\na1-->a2\nend\nsubgraph two\nb1-->b2\nend\nsubgraph three\nc1-->c2\nend", "flowchart LR\nc1-->a2\nsubgraph one\na1-->a2\nend\nsubgraph two\nb1-->b2\nend\nsubgraph three\nc1-->c2\nend"],
	["flowchart LR\nsubgraph TOP\ndirection TB\nsubgraph B1\ndirection RL\ni1 -->f1\nend\nsubgraph B2\ndirection BT\ni2 -->f2\nend\nend\nA --> TOP --> B\nB1 --> B2",
		"flowchart LR\nsubgraph TOP\ndirection TB\nsubgraph B1\ndirection RL\ni1 -->f1\nend\nB1 --> B2\nsubgraph B2\ndirection BT\ni2 -->f2\nend\nend\nTOP-->B\nA --> TOP"],
	["flowchart TB\nc1-->a2\nc1-->c2\nsubgraph one\na1-->a2\nend\nsubgraph two\nb1-->b2\nend\nsubgraph three\nc1\nc2\nend\none --> two\nthree --> two\ntwo --> c2",
		"flowchart TB\nc1-->a2\nc1-->c2\nsubgraph one\na1-->a2\nend\none --> two\nsubgraph two\nb1-->b2\nend\ntwo --> c2\nsubgraph three\nc1\nc2\nend\nthree --> two"],
	["flowchart LR\nid1>This is the text in the box]", "flowchart LR\nid1>This is the text in the box]"]
]) -> void:
	_test_parse_compose_transformed(initial_chart, expected_chart)


## Tests added for any unique bugs - like node type redefinition.
@warning_ignore("unused_parameter")
func test_parse_compose_special_failure_modes(initial_chart: String, expected_chart: String, expected_errors: int, test_parameters := [
	["flowchart LR\na-->b\na[text]", "flowchart LR\na[text]-->b", 0],
	["flowchart LR\na-->b\nb[[\"graph TD\nexit {exit node}\"]]\nexit-->c & d", "flowchart LR\na-->b[[\"graph TD\nexit {exit node}\"]]\nexit-->c\nexit-->d", 1]
]) -> void:
	_test_parse_compose_transformed(initial_chart, expected_chart, expected_errors)


func _test_parse_compose(chart: String, expected_errors := 0):
	_test_parse_compose_transformed(chart, chart, expected_errors)


func _test_parse_compose_transformed(initial_chart: String, final_chart: String, expected_errors := 0):
	Print.clear_errors()
	var m : MermaidParser = auto_free(MermaidParser.new())
	m.parse(initial_chart)
	var result = m.compose()
	if _clean_string(result) != _clean_string(final_chart):
		printerr("Mermaid parse failed! Expected the string \"%s\", but got \"%s\" instead!" % \
			[final_chart.replace("\n", "\\n"), result.replace("\n", "\\n")])
	assert_str(_clean_string(result)).is_equal(_clean_string(final_chart))
	assert_int(Print.error_count).is_equal(expected_errors)
	_cleanup(m)


func _clean_string(s: String) -> String:
	return s.strip_edges().replace(" ", "").replace("\t", "")


func _cleanup(m: MermaidParser):
	for id in m.nodes.keys():
		for link in m.nodes[id].links:
			auto_free(link)
