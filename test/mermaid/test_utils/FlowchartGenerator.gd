extends GdUnitTestSuite
class_name FlowchartGenerator


class MNode:
	const all_types := {
		MermaidNode.NodeType.STANDARD: "",
		MermaidNode.NodeType.DIALOGUE: "[...]",
		MermaidNode.NodeType.ROUNDED: "(...)",
		MermaidNode.NodeType.STADIUM: "([...])",
		MermaidNode.NodeType.SUBGRAPH: "[[...]]",
		MermaidNode.NodeType.CYLINDRICAL: "[(...)]",
		MermaidNode.NodeType.CIRCULAR: "((...))",
		MermaidNode.NodeType.COMMAND: "{...}",
		MermaidNode.NodeType.HEXAGON: "{{...}}",
		MermaidNode.NodeType.PARALLELOGRAM: "[\\...\\]",
		MermaidNode.NodeType.PARALLELOGRAM2: "[/.../]",
		MermaidNode.NodeType.TRAPEZOID: "[/...\\]",
		MermaidNode.NodeType.TRAPEZOID2: "[\\.../]",
		MermaidNode.NodeType.DOUBLECIRCLE: "(((...)))",
		MermaidNode.NodeType.FLAG: ">...]"
	}
	
	var id: String
	var text: String
	var type: MermaidNode.NodeType
	var next: Array[MNode] = []
	
	func _init(my_id: String, my_text: String, my_type: MermaidNode.NodeType):
		self.id = my_id
		self.text = my_text
		self.type = my_type
		# No text if we don't have a place for it.
		if type == MermaidNode.NodeType.STANDARD:
			self.text = ""

	func compose() -> String:
		return id + all_types[type].replace("...", text)


const allowed_gen_types := [
		MermaidNode.NodeType.STANDARD,
		MermaidNode.NodeType.DIALOGUE,
		MermaidNode.NodeType.ROUNDED,
		MermaidNode.NodeType.STADIUM,
		MermaidNode.NodeType.CYLINDRICAL,
		MermaidNode.NodeType.CIRCULAR,
		MermaidNode.NodeType.COMMAND,
		MermaidNode.NodeType.HEXAGON,
		MermaidNode.NodeType.PARALLELOGRAM,
		MermaidNode.NodeType.PARALLELOGRAM2,
		MermaidNode.NodeType.TRAPEZOID,
		MermaidNode.NodeType.TRAPEZOID2,
		MermaidNode.NodeType.DOUBLECIRCLE,
		MermaidNode.NodeType.FLAG
]

const characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
var previous_strings = {}
var nodes := []
var flowchart: String
var markdown: String:
	get:
		return "```mermaid\n%s\n```" % [flowchart]


func _init():
	flowchart = "flowchart TD"


# Unit test this tool so we don't see zero tests in the suite.
func test_random_string():
	for i in 100:
		var length := randi_range(1, 100)
		assert_str(random_string(length)).has_length(length)


func generate_flowchart(num_nodes: int):
	for i in range(num_nodes):
		nodes.append(_generate_node())
	
	# Line one: Use the full name of both nodes.
	if num_nodes > 0:
		flowchart += "\n%s" % [nodes[0].compose()]
	if num_nodes > 1:
		flowchart += "-->%s" % [nodes[1].compose()]
	if num_nodes > 2:
		for i in range(2, num_nodes):
			flowchart += "\n%s-->%s" % [nodes[i - 1].id, nodes[i].compose()]
			nodes[i-1].next.append(nodes[i])


## Put the first node in the chart.
func begin_chart(node_id: String, node_text: String, node_type := MermaidNode.NodeType.DIALOGUE):
	nodes = []
	flowchart = "flowchart TD"
	add_node(node_id, node_text, node_type)


func add_node(node_id: String, node_text: String, node_type := MermaidNode.NodeType.DIALOGUE):
	nodes.append(MNode.new(node_id, node_text, node_type))
	flowchart += "\n%s" % [nodes[0].compose()]


## Connects a node to the previously defined node.
func chain_node(node_id: String, node_text: String, node_type := MermaidNode.NodeType.DIALOGUE):
	var i := nodes.size()
	var node := MNode.new(node_id, node_text, node_type)
	nodes[i - 1].next.append(node)
	nodes.append(node)
	
	# Creating the second node.
	if i == 1:
		flowchart += "-->%s" % [node.compose()]
	else:
		flowchart += "\n%s-->%s" % [nodes[i - 1].id, node.compose()]


func start_subgraph(sg_name: String):
	flowchart += "\nsubgraph %s" % [sg_name]


func end_subgraph():
	flowchart += "\nend"


func assert_equals(mermaid: MermaidParser):
	assert_that(nodes.size() == mermaid.nodes.size()).override_failure_message("Graph should contain %s nodes, but has %s!" % [nodes.size(), mermaid.nodes.size()])
	if nodes.size() != mermaid.nodes.size():
		return
	
	for node in nodes:
		assert_dict(mermaid.nodes).contains_keys([node.id])
		if mermaid.nodes.has(node.id):
			_assert_node_equals(node, mermaid.nodes[node.id])


func random_string(length: int):
	var string = ""
	var characters_length = characters.length()
	while true:
		string = ""
		for i in range(length):
			var random_index = randi() % characters_length
			string += characters[random_index]
		if not previous_strings.has(string):
			break
	previous_strings[string] = true
	return string


func _assert_node_equals(test_node: MNode, mermaid_node: MermaidNode):
	_assert_node_equals_shallow(test_node, mermaid_node)
	
	# Check node outgoing links.
	assert_that(test_node.next.size() == mermaid_node.links.size()).override_failure_message("Node %s should have %d links, but has %s" % [test_node.id, test_node.next.size(), mermaid_node.links.size()])
	if test_node.next.size() == mermaid_node.links.size():
		for i in test_node.next.size():
			_assert_node_equals_shallow(test_node.next[i], mermaid_node.links[i].node)


func _assert_node_equals_shallow(test_node: MNode, mermaid_node: MermaidNode):
	# Check node values.
	assert_str("Node ID: " + test_node.id + "\nNode Text: " + test_node.text + "\nNode Type: " + MermaidNode.NodeType.keys()[test_node.type]). \
		is_equal("Node ID: " + mermaid_node.id + "\nNode Text: " + mermaid_node.raw_text + "\nNode Type: " + MermaidNode.NodeType.keys()[mermaid_node.type])


func _generate_node() -> MNode:
	var id = random_string(randi_range(2, 10))
	var text = random_string(randi_range(2, 10))
	var type = allowed_gen_types[randi_range(1, allowed_gen_types.size() - 1)]
	return MNode.new(id, text, type)
