# GdUnit generated TestSuite
class_name StoryInterfaceTest
extends GdUnitTestSuite
@warning_ignore('unused_parameter')
@warning_ignore('return_value_discarded')

# TestSuite generated from
const __source = 'res://mermaid/StoryInterface.gd'

func before():
	Print.silence_non_error_printing()


func before_test():
	Print.start_all()


func after_test():
	if is_failure():
		Print.from("MERMAID_PARSER", "Test failed! Dumping message history", Print.ERROR)
		Print.from("STORY_ENGINE", "Test failed! Dumping message history", Print.ERROR)


@warning_ignore("unused_parameter")
func test_story_interface_basic(chart: String, walk_path: Array, test_parameters := [
	# Single node.
	["flowchart TB\nA", [["A", null]]],
	# Multiple nodes.
	["flowchart TB\nA-->B-->C", [["A", 0], ["B", 0], ["C", null]]],
	# Branching paths.
	["flowchart TB\nA-->B & C-->D", [["A", 0], ["B", 0], ["D", null]]],
	["flowchart TB\nA-->B & C-->D", [["A", 1], ["C", 0], ["D", null]]],
	# Loopback.
	["flowchart TB\nA-->B-->A & C\nC-->D", [["A", 0], ["B", 0], ["A", 0], ["B", 1], ["C", 0], ["D", null]]],
]) -> void:
	_parse_traverse(chart, walk_path)


@warning_ignore("unused_parameter")
func test_story_interface_id(chart: String, walk_path: Array, test_parameters := [
	# Single node.
	["flowchart TB\nA", [["A", [], null]]],
	# Multiple nodes.
	["flowchart TB\nA-->B-->C", [["A", ["B"], 0], ["B", ["C"], 0], ["C", [], null]]],
	# Branching paths.
	["flowchart TB\nA-->B & C-->D", [["A", ["B", "C"], 0], ["B", ["D"], 0], ["D", [], null]]],
	["flowchart TB\nA-->B & C-->D", [["A", ["B", "C"], 1], ["C", ["D"], 0], ["D", [], null]]],
	# Loopback.
	["flowchart TB\nA-->B-->A & C\nC-->D", [["A", ["B"], 0], ["B", ["A", "C"], 0], ["A", ["B"], 0], ["B", ["A", "C"], 1], ["C", ["D"], 0], ["D", [], null]]],
]) -> void:
	_parse_traverse_check_comments(chart, walk_path)


@warning_ignore("unused_parameter")
func test_story_interface_comments(chart: String, walk_path: Array, test_parameters := [
	# Single node.
	["flowchart TB\nA[Node A]", [["Node A", [], null]]],
	# Multiple nodes.
	["flowchart TB\nA[Node A]-->B[Node B]-->C[Node C]", [["Node A", [""], 0], ["Node B", [""], 0], ["Node C", [], null]]],
	# Branching paths.
	["flowchart TB\nA[Node A]-->B[Node B] & C[Node C]-->D[Node D]", [["Node A", ["Node B", "Node C"], 0], ["Node B", [""], 0], ["Node D", [], null]]],
	["flowchart TB\nA[Node A]-->B[Node B] & C[Node C]-->D[Node D]", [["Node A", ["Node B", "Node C"], 1], ["Node C", [""], 0], ["Node D", [], null]]],
]) -> void:
	_parse_traverse_check_comments(chart, walk_path)


@warning_ignore("unused_parameter")
func test_story_interface_link_text(chart: String, walk_path: Array, test_parameters := [
	# Single node.
	["flowchart TB\nA", [["A", [], null]]],
	# Multiple nodes - inline.
	["flowchart TB\nA-- goto b -->B-- goto c -->C", [["A", ["goto b"], 0], ["B", ["goto c"], 0], ["C", [], null]]],
	# Multiple nodes with quotations - inline.
	["flowchart TB\nA-- \"goto b\" -->B-- \"goto c\" -->C", [["A", ["goto b"], 0], ["B", ["goto c"], 0], ["C", [], null]]],
	# Multiple nodes - |pipe|.
	["flowchart TB\nA-->|goto b|B-->|goto c|C", [["A", ["goto b"], 0], ["B", ["goto c"], 0], ["C", [], null]]],
	# Multiple nodes with quotations - |pipe|.
	["flowchart TB\nA-->|\"goto b\"|B-->|\"goto c\"|C", [["A", ["goto b"], 0], ["B", ["goto c"], 0], ["C", [], null]]],
	# Loopback.
	["flowchart TB\nA-->|goto b|B-->|go back|A\nB-->|goto c|C\nC-->|finish|D",
		[["A", ["goto b"], 0], ["B", ["go back", "goto c"], 0], ["A", ["goto b"], 0], ["B", ["go back", "goto c"], 1], ["C", ["finish"], 0], ["D", [], null]]],
]) -> void:
	_parse_traverse_check_comments(chart, walk_path)


@warning_ignore("unused_parameter")
func test_story_interface_subgraph_node(chart: String, walk_path: Array, test_parameters := [
	# Single node in subgraph.
	["flowchart TB\nsg[[\"flowchart TB\nA\nexit{B}\"]]-->B", [["A", null]]],
	# Multiple nodes within graph.
	["flowchart TB\nsg[[\"flowchart TB\nA-->exit{B}\"]]-->C", [["A", 0], ["C", null]]],
]) -> void:
	_parse_traverse(chart, walk_path)


@warning_ignore("unused_parameter")
func test_story_interface_mermaid_subgraph(chart: String, walk_path: Array, test_parameters := [
	# Single node in subgraph.
	["flowchart TB\nsubgraph A\nB\nend", [["B", null]]],
	# Multiple nodes within graph.
	["flowchart TB\nsubgraph A\nB-->C\nend", [["B", 0], ["C", null]]],
	# Multiple nodes with transitions outside graph.
	["flowchart TB\nB\nsubgraph A\nB-->C\nend", [["B", 0], ["C", null]]],
	["flowchart TB\nB-->C\nsubgraph A\nB\nend", [["B", 0], ["C", null]]],
	["flowchart TB\nsubgraph A\nB\nend\nB-->C", [["B", 0], ["C", null]]],
]) -> void:
	# Errors will print because we have subgraphs with no exit defined.
	Print.silence_all()
	_parse_traverse(chart, walk_path)


func _parse_traverse(chart: String, walk_path: Array):
	var mermaid := MermaidParser.new(chart)
	var story := StoryInterface.new(mermaid)
	
	# Walk through one path and make sure that we get the answer we want.
	for i in walk_path.size():
		assert_str(story.text).is_equal(walk_path[i][0])
		if walk_path[i][1] != null:
			story.advance(walk_path[i][1])


func _parse_traverse_check_comments(chart, walk_path):
	var mermaid := MermaidParser.new(chart)
	var story := StoryInterface.new(mermaid)
	
	# Walk through one path and make sure that we get the answer we want.
	for i in walk_path.size():
		assert_str(story.text).is_equal(walk_path[i][0])
		assert_array(story.options).is_equal(walk_path[i][1])
		if walk_path[i][2] != null:
			story.advance(walk_path[i][2])
	if is_failure():
		pass
