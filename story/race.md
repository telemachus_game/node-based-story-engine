# Race
```mermaid
graph TD
	enter{Start}
	enter --> a["You race for 3 laps. Each lap consists of 2 combat zones and 2 overtaking zones.\nWhat is the outcome of the race?"]
	a -->|I win the race| exit_0{Win}
	a -->|I finish the race| exit_1{End}
	a -->|I die in combat| exit_2{Die}
```