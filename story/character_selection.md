# Character Selection

```mermaid
graph TD
	enter{Start}
	exit{End}
	enter --> a[A line of gladiators are waiting to enter the colosseum. Select one to be your character. Above your character, you can see people entering and exiting the colosseum. This scene will change as the story progresses.]
	b[You enter the colosseum. The gate shuts behind you, and a moment later, your chains fall off. You are now a gladiator.]
	a -. "The strong one" .-> b
	a -. "The tall one" .-> b
	a -. "The short one" .-> b
	a -. "The old man" .-> b
	a -. "The child" .-> b
	b --> c[The guards point you towards a training room where an old projector is displaying information about the race vehicle. One of the other contestants continues down the hallway. The guard gives him a quick glance, but says nothing.]
	c -. "Enter the training room." .-> d
	d[You watch a training video before continuing down the hallway. This explains the game's controls.]
	c -. "Continue down the hallway" .-> e
	e[There are a number of vehicles in the room. Some look new, others are damaged, and one appears to have been heavily modified.]
	e -.->|"Enter the new vehicle"| z
	e -.->|"Enter the vehicle with clear damage"| z
	e -.->|"Enter the vehicle with a modified weapon system"| z
	e -.->|"Enter the vehicle with extensive modifications"| z
	e -.->|"Enter the old-looking vehicle"| z
	d --> f[There are two vehicles in the room. One appears to be damaged, and the other one looks quite old.]
	f -.->|"Enter the old-looking vehicle"| z
	f -.->|"Enter the vehicle with clear damage"|z
	z[You enter the vehicle. The cockpit seals, and a conveyor system begins to roll you towards the staging area. Your first race is about to begin.]
	z --> exit
```