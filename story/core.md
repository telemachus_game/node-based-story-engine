# Core game loop

Wipeout/F0 racing game with air battle segments a la Star Fox.

Permadeath racer, if you lose a race, you lose your character.

Story progresses regardless of character deaths.

If you take enough damage, you lose power. You have to re-route power (small mini-game) in under 10 seconds. If you take too long, you get eliminated by a kill vehicle (3...2...1...Eliminated. Kill Vehicle Dispatched)

You can win by being set free (after killing enough opponents), or by helping the old man end the games for everyone.

Old man plays the role of Telemachus. He entered the games as a freedman, and refuses to fight, though he races well and has earned the respect of all.

Immortals are allowed to race remotely, ensuring that they never die. Earning enough respect grants you the rank of immortal.

After winning enough matches, you get to race Telemachus. The immortals ask you to honor him by racing fairly and avoiding combat. You take damage (scripted sequence) and he swerves into the kill vehicle in order to save your life. You win by elimination.

World has three barrack towns. Shops are in the center.

### Game modes:

- Lap Place Elimination (you get eliminated if in too low of a place)
- Death race - Race until one racer returns, or 100 laps are reached.
- Standard race - Win by place or combat
- Hoard - You try to complete X laps while a team of racers tries to kill you
- Hoard elimination - You kill the hoard or they kill you
- Team tournament - Score assigned by racers in the top 5 spots

```mermaid
flowchart TD
	enter{Start} --> a
	a["Welcome to Telemachus! This is a narrative demo for the game. Actual gameplay will be represented through text.
In this early version, you can either simulate a game loop, or view the story draft."]
	a-->|Game Demo|b[[character_selection.md]]
	a-->|Story Demo|sg1[[story.md]]
	b-->c[[race.md]]
	c-->|Win|d
	d[(You gained reputation)]
	c -->|Lose|e[(You lost reputation)]
	h[[town.md]]
	h --> c
	d & e --> f
	f[note: these nodes will evaluate variables in a later version\nHave you gained enough reputation to be set free? Alternatively, has the narrative reached a conclusion?]
	f-->|Yes|g
	g[You win!]
	f-->|no|h
	c -->|You died| b
```
