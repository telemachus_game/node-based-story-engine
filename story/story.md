# Story events and branching.
Game is a series of loops:

- The story progresses linearly from the introduction of the games to the death of Telemachus and eventually to the ban of all death racing.
- The player characters will enter and exit the story as they progress their careers and die.
- Each race, each character is able to make choices (how they conduct themselves on the track). Small narrative events will guide the player's decisions,

### Telemachus conversations:
A slave who sacrifices virtue in order to gain his freedom is likely to become a crueler tyrant than the master he once served.

### The prevailing worldview:
"Our ancestors were weak. Their idealism blinded them, and their worst enemies lurked within their own borders."
"Death is inevitable. You can cower until it finds you, or you can make it your ally and wield it as you forge an empire."

Telemachus: Based on a true story

## Chapter 1: The Games
- Game start: Select your character from a chain-gang of slaves. This is happening directly below the Colosseum entrance. Above, you can see people coming and going.
- Walk through a set of rooms, each one is optional.
    - Introduction to the races - animated video
        - Today, you will give your life for the glory of the Empire. In the next room is the GN-109 maglev chariot. Rome Transport has designed this vehicle from the top-down for high speed combat racing. Here is a brief overview of the vehicle's controls.
    - Select a vehicle - first come, first serve. Some are better than others.
    - Compete in your first race.
- Enter the hub-world.
- Compete in more races.
- Get to know the other players.
- Get to know Telemachus.
- Die in the races and re-roll multiple players.

## Chapter 2: Telemachus
- Every year, the Emperor and the People elect one champion each to earn their freedom. Enter a house, then race well to be set free.
- Player progresses far enough to enter the Immortal house. (Three houses, People's Champion, Emperor's Immortals, Common Racers, along with sponsored free men) All start in Common Racers.
    - Welcome to the Immortal House. Your job is to tell the Emperor's narrative (essentially pro wrestling)
    - As you talk to the Immortals, realize that each one has a personal story with Telemachus
        - Telemachus is a free man racing in the People's Champion's house. He lives with the slaves, and he has dedicated his life to preserving the lives of the slaves.
        - His reputation of nonviolence means that people don't attack each other when he is racing (usually)

## Chapter 3: The Change
- Telemachus dies while protecting your character. His public death leads to a strong movement to stop the games. Protests start outside of the arena, and your actions inside the arena become central to the growing struggle.
- You are offered freedom from the Emperor if you help him suppress the movement.
- There are a few possible endings:
    1.  You chose to side with the freedom movement. You are strongly opposed by the Emperor, but you eventually win out, leading to the ban on the games.
    2.  You side with the Emperor. You race in a number of scripted-event races, and you are granted your freedom. The fight continues, but you don't see the ending.
    3.  You die and start over as a relative outsider. You participate in the movement, but are not central to it. You either earn your freedom, or you see the movement reach its conclusion.

```mermaid
flowchart TD
    start{Start} --> intro

    subgraph intro [Introduction]
    i1["You are a gladiator in the combat races in the Colosseum. You can die in the race.
If that happens, you will start over with a new character. The story will focus on the
relationship between all slaves and the Empire, so the narrative will continue through all of your deaths."]
    i1-->i2["You will race in Great Babel, the capital city of the New Earth Empire.
Your death will bring glory to the Empire, and will promote stability and strength across the world."]
    i2-->i3["If you manage to endure, you can become a member of the People's Champions or the Emperor's Immortals.
Every year, one Champion and one Immortal are granted their freedom. You win the game if you become either.
Some free men become racers in order to earn fame and glory, but the majority of racers are slaves."]
    i3-->i4["In the midst of this, there is an old man named Telemachus. He is a free man, and he is in the Colosseum, but
he is not a mercenary. He is a political dissident, and he believes that every life matters, whether slave or free.
The authorities don't really know what to do with him because prisoners are often executed in the Colosseum, but
Telemachus entered the colosseum willingly in an attempt to stop the violence"]
    end
    
    i4-->tDeath
    
    subgraph tDeath [Telemachus Dies]
    t1["TODO: Expand this section. Telemachus dies in the place of your character. His death ignites larger political controversy,
and it begins to shine a light on the plight of the gladiators. Your current character is in the center of the emerging struggle"]
    end

    t1-->struggle

    subgraph struggle [The branchy bits]
    s1["You are thrust into the spotlight as a result of Telemachus's death. The Emperor offers you your freedom if you will follow his script.
Refuse, and he promises to make your death swift and humiliating.
(Note: The games are like Pro Wrestling at the high level. The actions of Immortals are fully scripted by the government, but People's Champions are not.)"]
    s1-->|"Accept the Emperor's offer."|si1
    s1-->|"Refuse"|sp1

    %% Immortal route
    si1["You are privately honored for causing the death of Telemachus. You kill an Immortal in a scripted race and take his place.
You are asked to perform multiple execution tasks as you tell the Emperor's story. If you continue to 
follow his script, then you will eventually be granted your freedom."]
    si1---->|"Follow the Emperor's script. (includes a lot of murder)"|si2
    si2["After faithfully fighting the put down the dissidents, you are finally granted the honor of release.
You are given the name ''The Patriot Immortal,'' though the people prefer to call you ''The Executioner.''
Your final race is an execution event. You will kill 10 People's Champions. They do not fight back."]
    si2-->|"Kill them all!"|si3
    si3["After the race, you are ceremoniously granted your freedom. You board a hover-carriage and exit through the front door.
As you leave, you pass multiple riots. It appears that the Emperor has more work to do if he wishes to restore order."]
    si2--->|"Spare them"|si4
    si4["The crowds get increasingly agitated as the laps stretch on. It is clear that this is a protest, and you have chosen to
take part. Riots begin in the city as freemen begin to ally with the plight of the gladiators.
Eventually, the race is called off and the Emperor is forced to capitulate. Every living gladiator is released."]
    
    si1-->|"Refuse to follow the Emperor's script at any point."|sn1
    si1-->|"Die during a race, and start over with a new character"|sd1


    %% People's Champion Route
    sp1["You are accepted into the community of People's Champions. You hear from an outside correspondent that Telemachus has begun
to turn the hearts of the people. The PCs recognize the opportunity and take a collective vow of nonviolence.
TODO: Expand this path."]
    sp1------>sf1


    %% Neutral Route
    sn1["You refuse to take a side, and so you take a backseat role to the revolution. You race for yourself, and tell your own story as you progress."]
    sn1--->sf1


    %% Death Route
    sd1["You enter the story as an outsider.
TODO: Expand this path. You are invited into the story as a part of the growing revolution. You can either take part in protest 
races, or you can kill dissidents. You will be asked to join either the People's Champions, or the Immortals as a result of your actions."]
    sd1-->sf1


    %% Final Race
    sf1["The Emperor is tired of the dissidents trying to use his games to tell their own story. He arranges a final execution match.
The dissidents refuse to fight, and the crowds grow increasingly angry as they are killed. Riots begin to break out in the city,
and the Emperor is forced to capitulate."]
    end


    %% Fin.
    si3--->ec1["End Credits. Thank you for playing."]
    sf1 & si4-->ec2["End Credits.


On January 1st, 404 AD. a monk named Telemachus gave his life to stop the Games. In response to his death, Emperor Honorius officially abolished gladiatorial combat.

To this day, the last drop of blood to dry on the Colosseum floor was the blood of a free man, given willingly to save the lives of those who were condemned to die.

Thank you for playing."]
```
