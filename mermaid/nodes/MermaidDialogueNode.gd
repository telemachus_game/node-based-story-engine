extends MermaidNode
class_name MermaidDialogueNode


func _init(node_id: String, node_text: String = ""):
	super._init(node_id, node_text, NodeType.DIALOGUE)


func get_text() -> String:
	return text


## If there is one outgoing link, returns the link note or an empty string.
## If there are multiple links, returns the link note or node text for each link.
func get_link_options() -> Array[String]:
	var options: Array[String] = []
	if links.size() == 1:
		options.append(links[0].note)
	else:
		for link in links:
			if link.note == "":
				options.append(link.node.get_text())
			else:
				options.append(link.note)
	return options


func get_next_node(link_num) -> MermaidNode:
	return links[link_num].node
