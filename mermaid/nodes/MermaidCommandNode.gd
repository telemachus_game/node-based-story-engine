extends MermaidNode
class_name MermaidCommandNode

signal node_activated


func _get_can_auto_advance() -> bool:
	return links.size() > 0


func _init(node_id: String, node_text: String = ""):
	super._init(node_id, node_text, NodeType.COMMAND)


func get_text() -> String:
	run_log.error("Command node had its text accessed. Command nodes should auto-advance!")
	return ""


func get_link_options() -> Array:
	run_log.error("get_link_options was called on a Command node. Command nodes should auto-advance!")
	return []


func get_next_node(link_num) -> MermaidNode:
	run_log.assert_that(link_num >= 0 and link_num <= links.size(), 
		"Attempted to get a node that doesn't exist! Link %s accessed, but node %s only has %s links!" % [link_num, id, links.size()])
	return links[link_num].node


func auto_advance() -> MermaidNode:
	emit_signal("node_activated")
	run_log.assert_that(can_auto_advance, "Node %s is not auto-advancing! Check can_auto_advance before calling auto_advance!")
	return get_next_node(0)
