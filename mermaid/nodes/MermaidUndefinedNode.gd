extends MermaidNode
class_name MermaidUndefinedNode


func _init(node_id: String, node_text: String, node_type: NodeType):
	super._init(node_id, node_text, node_type)


func get_text() -> String:
	return text


func get_link_options() -> Array[String]:
	var options: Array[String] = []
	for link in links:
		if link.note == "":
			options.append("Continue")
		else:
			options.append(link.note)
	return options


func get_next_node(link_num) -> MermaidNode:
	return links[link_num].node
