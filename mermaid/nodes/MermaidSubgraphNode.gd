extends MermaidNode
class_name MermaidSubgraphNode

var base_dir := ""
var exit_num := 0

var _in_subgraph := false
var _auto_advance := false
var _mermaid: MermaidParser
var mermaid: MermaidParser:
	get:
		return _mermaid

func _get_can_auto_advance():
	return _auto_advance


func _init(node_id: String, node_text: String, node_type := NodeType.SUBGRAPH):
	super._init(node_id, node_text, node_type)
	parse_log.assert_that(node_type == NodeType.SUBGRAPH or node_type == NodeType.INLINE, "Tried to create a subgraph node from type " + NodeType.keys()[node_type])
	_mermaid = MermaidParser.new("", true)


func ready():
	# We are setting this node up in conjunction with the parent flowchart.
	if type == NodeType.INLINE:
		if _mermaid.start_node == null:
			parse_log.error("Submodule %s has no defined start node! Traversal will stall." % [id])
		else:
			_auto_advance = true
			parse_log.debug("Auto Advance enabled for subgraph " + id)
		return
	
	if MermaidParser.is_string_valid_mermaid(text):
		_mermaid = MermaidParser.new(text, true)
	elif base_dir != "":
		_mermaid = MermaidParser.load_from_file(base_dir.path_join(text), true)
	if _mermaid:
		if _mermaid.exit_nodes.size() != links.size() and type != NodeType.INLINE:
			parse_log.error("Subgraph %s must have the same number of exit nodes as outgoing links! Has %s exit nodes and %s links." % [text, _mermaid.exit_nodes.size(), links.size()])
		# Connect end nodes back in to this node.
		for i in _mermaid.exit_nodes.size():
			_mermaid.exit_nodes[i].connect("node_activated", _get_exit_num.bind(i))
			_mermaid.exit_nodes[i].add_link(self, str(i), MermaidLink.LinkType.DYNAMIC)
		
		_auto_advance = (_mermaid.start_node != null)
		parse_log.debug("Subgraph %s auto-advance set to %s" % [id, can_auto_advance])
	else:
		parse_log.error("Mermaid file %s could not be opened. Subgraph will be skipped." % [base_dir.path_join(text)])
		# We will automatically "exit" the graph upon entering it.
		_in_subgraph = true


func compose(print_details := true) -> String:
	match type:
		NodeType.INLINE:
			if print_details:
				if _in_subgraph:
					_in_subgraph = false
					return "end"
				else:
					_in_subgraph = true
					return "subgraph " + id
			else:
				return id
		NodeType.SUBGRAPH:
			return super.compose(print_details)
		_:
			parse_log.error("Subgraph node %s has invalid type %s! Returning empty string instead." % [id, NodeType.keys()[type]])
			return ""


func get_text() -> String:
	run_log.error("Attempted to read subgraph %s as text. The node should have auto-advanced to prevent this." % [id])
	return text


func get_link_options() -> Array[String]:
	var options: Array[String] = []
	for link in links:
		if link.note == "":
			options.append("Continue")
		else:
			options.append(link.note)
	return options


func auto_advance() -> MermaidNode:
	if _in_subgraph:
		_in_subgraph = false
		_auto_advance = (_mermaid.start_node != null)
		return get_next_node(exit_num)
	else:
		_in_subgraph = true
		run_log.assert_that(_mermaid.start_node != null, "The subgraph does not contain a valid start node!")
		if links.size() == 0:
			_auto_advance = false
			run_log.info("Subgraph %s has no valid exits! Parsing will stop in the subgraph." % [id])
		return _mermaid.start_node


func get_next_node(link_num) -> MermaidNode:
	run_log.assert_that(links.size() >= link_num - 1, "Mermaid subgraph %s does not have an exit %s!" % [text, link_num])
	return links[link_num].node


func _get_exit_num(index: int):
	exit_num = index
