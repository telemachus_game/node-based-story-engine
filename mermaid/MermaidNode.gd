extends Resource
## A class representing a node in a mermaid diagram.
## This is a base class, each node type should be 
class_name MermaidNode

## Enum representing the type of node.
enum NodeType {
	STANDARD,       # Node with no defined shape. id
	INLINE,         # Subgraph, not a regular node. subgraph id
	DIALOGUE,       # Regular rectangular node. id[x]
	COMMAND,        # Rhombus node. id{x}
	SUBGRAPH,       # Box node. id[[x]]
	ROUNDED,        # Node with rounded edges. id(x)
	STADIUM,        # Stadium-shaped node. id([x])
	CYLINDRICAL,    # Cylindrical node. id[(x)]
	CIRCULAR,       # Circular node. id((x))
	HEXAGON,        # Hexagon node. id{{x}}
	PARALLELOGRAM,  # Parallelogram node. id[/x/]
	PARALLELOGRAM2, # Alt Parallelogram node. id[\x\]
	TRAPEZOID,      # Trapezoid node. id[/x\]
	TRAPEZOID2,     # Alt Trapezoid node. id[\x/]
	DOUBLECIRCLE,   # Double circle node. id(((x)))
	FLAG,           # Flag-shaped node. id>x|
}

## The unique identifier for the node.
var id: String

## The text label for the node. If no text is provided, the ID is used as the label.
## Text has parentheses and text newlines removed.
var text: String:
	get:
		return raw_text.replace("\"", "").replace("\\n", "\n")

## The text label for the node with parentheses and newlines left in.
var raw_text := ""

## An array storing the links connected to the node.
var links: Array

## The shape of the node, represented by the NodeType enum (rectangle, diamond, etc.).
var type: NodeType

# My reference to the MERMAID_PARSER logger
var parse_log: Logger
# My reference to the STORY_ENGINE logger
var run_log: Logger

## True if this node is capable of advancing to the next node automatically.
## False if this node needs to be advanced via get_next_node.
var can_auto_advance: bool: get = _get_can_auto_advance
func _get_can_auto_advance() -> bool:
	return false


# Returns the id, type, and text on a node.
# If the node does not contain text, the id and text will be the same.
static func parse_node(node: String) -> Array:
	var node_id = node.strip_edges()
	var node_type = MermaidNode.NodeType.STANDARD
	var node_text = node.strip_edges()
	var shapes = {
		"(.*)\\[\\[(.*)\\]\\]": MermaidNode.NodeType.SUBGRAPH,
		"(.*)\\[\\/(.*)\\\\\\]": MermaidNode.NodeType.TRAPEZOID,
		"(.*)\\[\\\\(.*)\\/\\]": MermaidNode.NodeType.TRAPEZOID2,
		"(.*)\\[\\\\(.*)\\\\]": MermaidNode.NodeType.PARALLELOGRAM,
		"(.*)\\[\\/(.*)\\/\\]": MermaidNode.NodeType.PARALLELOGRAM2,
		"(.*)\\(\\(\\((.*)\\)\\)\\)": MermaidNode.NodeType.DOUBLECIRCLE,
		"(.*)\\(\\((.*)\\)\\)": MermaidNode.NodeType.CIRCULAR,
		"(.*)\\(\\[(.*)\\]\\)": MermaidNode.NodeType.STADIUM,
		"(.*)\\[\\((.*)\\)\\]": MermaidNode.NodeType.CYLINDRICAL,
		"(.*)\\{\\{(.*)\\}\\}": MermaidNode.NodeType.HEXAGON,
		"(.*)\\{(.*)\\}": MermaidNode.NodeType.COMMAND,
		"(.*)>(.*)\\]": MermaidNode.NodeType.FLAG,
		"(.*)\\((.*)\\)": MermaidNode.NodeType.ROUNDED,
		"(.*)\\[(.*)\\]": MermaidNode.NodeType.DIALOGUE,
		"(.*)": MermaidNode.NodeType.STANDARD,
	}
	
	for shape in shapes.keys():
		var regex = RegEx.new()
		regex.compile(shape)
		var match = regex.search(node)
		if match:
			node_type = shapes[shape]
			node_id = match.get_string(1).strip_edges()
			node_text = match.get_string(2).strip_edges()
			break
	
	return [node_id, node_type, node_text]


## Initializes a new node with given id, text, and type.
func _init(node_id: String, node_text: String = "", node_type: NodeType = NodeType.STANDARD):
	self.id = node_id
	self.raw_text = node_text
	self.type = node_type
	self.links = []
	self.parse_log = Print.get_logger("MERMAID_PARSER", true)
	self.run_log = Print.get_logger("STORY_ENGINE", true)


## Initialize this subnode once all links have been added.
func ready():
	parse_log.error("ready called on %s node. This node type does not have anything to initialize." % NodeType.keys()[type])


## Compose this node into a string. Does not include links.
func compose(print_details := true) -> String:
	var node_type_dict = {
		MermaidNode.NodeType.STANDARD: "",
		MermaidNode.NodeType.INLINE: "",
		MermaidNode.NodeType.DIALOGUE: "[...]",
		MermaidNode.NodeType.ROUNDED: "(...)",
		MermaidNode.NodeType.STADIUM: "([...])",
		MermaidNode.NodeType.SUBGRAPH: "[[...]]",
		MermaidNode.NodeType.CYLINDRICAL: "[(...)]",
		MermaidNode.NodeType.CIRCULAR: "((...))",
		MermaidNode.NodeType.COMMAND: "{...}",
		MermaidNode.NodeType.HEXAGON: "{{...}}",
		MermaidNode.NodeType.PARALLELOGRAM: "[\\...\\]",
		MermaidNode.NodeType.PARALLELOGRAM2: "[/.../]",
		MermaidNode.NodeType.TRAPEZOID: "[/...\\]",
		MermaidNode.NodeType.TRAPEZOID2: "[\\.../]",
		MermaidNode.NodeType.DOUBLECIRCLE: "(((...)))",
		MermaidNode.NodeType.FLAG: ">...]"
	}
	
	var node_type_str = node_type_dict[type]
	if raw_text == id or not print_details:
		return id
	else:
		return id + node_type_str.replace("...", raw_text)


## Adds a link to the node with specified node to link to, note, direction, type, arrow, and length.
func add_link(node: MermaidNode, note: String = "", linkType: MermaidLink.LinkType = MermaidLink.LinkType.SOLID, direction: MermaidLink.LinkDirection = MermaidLink.LinkDirection.UNIDIRECTIONAL, arrow: MermaidLink.ArrowType = MermaidLink.ArrowType.ARROW, length := 1):
	var link = MermaidLink.new(direction, linkType, arrow, length, note, node)
	self.links.append(link)
	
	# If the link is bidirectional, a back_link is also created from the target node to the current node.
	if direction == MermaidLink.LinkDirection.BIDIRECTIONAL:
		var back_link = MermaidLink.new(direction, linkType, arrow, length, note, self)
		node.links.append(back_link)
		parse_log.verbose("Created bidirectional link from %s to %s. Link note: \"%s\"" % [id, node.id, note])
	else:
		parse_log.verbose("Created link from %s to %s. Link note: \"%s\"" % [id, node.id, note])


## Returns the text on this node. (Some variants may generate the text before returning it)
func get_text() -> String:
	run_log.warning("Node " + id + " has been traversed. This is a standard node with no text. Consider using a specific node type instead!")
	return id


## Returns a list of outgoing nodes.
func get_link_options() -> Array[String]:
	var options: Array[String] = []
	for link in links:
		if link.note == "":
			options.append(link.node.get_text())
		else:
			options.append(link.note)
	return options


## Jumps to the next node. If link_num is provided, it will jump to the selected node.
## For a list of nodes to jump to, call get_link_options. The link option index is used to select the next node.
func get_next_node(link_num) -> MermaidNode:
	return links[link_num].node


## Jump to the next node automatically.
## Will throw an error if can_auto_advance is false.
func auto_advance() -> MermaidNode:
	run_log.error("The default Mermaid node CANNOT auto-advance. Use get_next_node instead!")
	return null
