```mermaid
flowchart TD
    start{start}-->A
    A["Welcome to the self-documenting story engine demo. Press ''continue'' to advance to the next node."]
    A -->|Continue| B["This demo is actually a collection of markdown files. We are in /addons/mermaid/core.md right now."]
    A -->|Skip the Preamble| j
    B --> C["Each type of node will perform a specific action. Boxes ''[ ]'' create dialogue nodes."]
    C --> D["Nodes can have multiple destinations. Links (-->) can have text - (--text-->) or (-->|text|) - which can display as an option."]
    D -->|Wow! That's interesting.| E
    D -->|So, I get dialogue choices?| E
    E[Yup.] --> j

    %% Jump table.
    j["What feature do you want to learn about? This demo documents story engine features, but you may want to visit [url=https://mermaid.js.org/syntax/flowchart.html]the Mermaid docs[/url] for more information on Mermaid itself."]
    jmp["What feature do you want to learn about?"]
    j & jmp -.->|I'm done| zz[That's all! Thank you for exploring.]-->exit{exit}
    j & jmp -.->|How do I keep my scripts modular?| s1 --> jmp
    j & jmp -.->|How do variables work?| s2 --> jmp
    j & jmp -.->|Can you explain command nodes?| s3 --> jmp
    j & jmp -.->|How do you make these options disappear?| s4 --> jmp
    j & jmp -..->|Wait, how did you get a URL in your dialogue?| bb["Oh, that was just [url=https://docs.godotengine.org/en/stable/tutorials/ui/bbcode_in_richtextlabel.html]BBCode[/url]. It's not actually a part of the story engine."]-->|Oh, ok.| jmp

    s1[[subgraphs.md]]
    s2[[variables.md]]
    s3[[commands.md]]
    s4[[links.md]]
```