```mermaid
flowchart TD
    start{Start} --> s1
    s1["You can split your big story up into little subgraphs. Subgraphs can be handled a few different ways. You can use the ''subgraph'' command to create a box. You can also use a subroutine node ''[[ ]]'' to load a subgraph from a file."]
    s1 --> s2[What type of subgraph do you want to explore?]
    s2 --> |Let's go to a subgraph file.| s3
    
    %% Subgraph file.
    s3[We already did. We are currently in the subgraph.md file.]
    s3 --> s4["Subgraph files usually have a start node and an end node."]
    s4 --> s5[If you create a dialogue node with no outgoing links, it will end the story.]
    s5 --> |Oh, really?|s6[Yup. Now you're stuck here.]
    s5 --> |How do we exit subgraph files, then?|s8
    s8["That's the neat part, you don't!\nJust kidding, you just have to define an exit node: exit{exit node}"]
    s8 --> exit

    %% Mermaid subgraph.
    s2 <-->|Let's go to an inline subgraph|sg1
    subgraph sg1
    ss1[There are two ways to enter subgraph. When you jump to the graph itself, you will start at the first node to be defined, or the Start node, if it is explicitly defined.]
    ss1 --> ss2[In this case, the first node in the subgraph will be displayed. However, for subgraphs that are created with the ''subgraph'' command, you can also jump directly to a node in the graph.]
    ss2 -->|Let's exit this graph.| ss3
    ss3[When you reach an exit node, the subgraph will exit to its first valid link.]
    ss3 --> ss4["Normally, a node with no outgoing links will end the dialogue. However, End nodes are the exception and will force the subgraph to jump to one of its links."]
    ss4 --> ss5["I have defined two exit nodes. They will line up with the subgraphs two exit links, in order of definition. Make sure to define link 0 first, or the wires will get crossed."]
    ss5 -->|Let's go back and explore more subgraphs.| exit_0
    exit_0{Return}
    ss5-->|Let's stop talking about subgraphs.|exit_1{Leave}
    end

    %% Special note: Nodes will get shoved into a subgraph if you call them before the end statement. If you want to link to the outside, make sure to do it after "end".
    ss2 ------>|Let's jump into another graph!| ssa
    
    %% Wondering why exit_0 goes back to node s2 even though this line is the only clearly defined exit? It's because we defined the s2<-->sg1 link as a bidirectional link! These tricks can be fun, but be careful to not overdo it! It's easy to make a graph impossible to understand at a glance.
    sg1 --> exit

    %% More Mermaid Subgraphs.
    subgraph sg2
    ssa["We just jumped directly into a different subgraph. This time, the subgraph itself is not reachable, but the nodes inside are."]
    end
    ssa-->exit

    exit{Exit}
```