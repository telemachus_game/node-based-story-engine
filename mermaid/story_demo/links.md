```mermaid
flowchart TD
    start{Start}-->a[That feature is still in development.]-->exit{End}
    %% Basically, dotted links are "fragile" and will be destroyed once traversed.
    %% Other link types are not defined yet, but I'll do something with them if I ever need to.
```