extends Resource
class_name MermaidParser

var nodes: Dictionary = {} # {id: node}
var start_node: MermaidNode
var exit_nodes: Array[MermaidNode]
var orientation := "TD"
var logger: Logger
var file_path: String
var active_subgraph: MermaidParser = null
var shared_nodes: Dictionary = {}


## If a string is provided, parses the string.
func _init(flowchart := "", is_subgraph := false):
	logger = Print.create_logger("MERMAID_PARSER", Print.INFO, Print.VERBOSE)
	if !is_subgraph:
		logger.start()
	if flowchart != "":
		parse(flowchart)


## Checks if the input string is a valid MermaidParser flowchart.
## Only does a preliminary check. Flowcharts may have errors that are uncaught by this function.
static func is_string_valid_mermaid(chart: String):
	var types = ["flowchart", "graph"]
	var orientations = ["TB", "TD", "BT", "RL", "LR"]
	
	for type in types:
		for dir in orientations:
			if chart.begins_with(type + " " + dir):
				Print.from("MERMAID_PARSER", "Mermaid string is valid", Print.DEBUG)
				return true
	
	if chart == "":
		Print.from("MERMAID_PARSER", "Mermaid string is empty!")
	else:
		Print.from("MERMAID_PARSER", "Mermaid string is invalid:\n" + chart)
	return false


## Loads a Mermaid chart from a valid Markdown file. 
static func load_from_file(path: String, is_subgraph := false) -> MermaidParser:
	var file = FileAccess.open(path, FileAccess.READ)
	Print.from("MERMAID_PARSER", "Loading from file: " + path)
	if file == null:
		Print.from("MERMAID_PARSER", "File %s failed to open!" % [path], Print.ERROR)
		return null
	
	var file_text = _strip_markdown(file.get_as_text())
	if !is_string_valid_mermaid(file_text):
		return null
	
	var mermaid = MermaidParser.new("", is_subgraph)
	mermaid.file_path = path
	mermaid.parse(file_text)
	return mermaid


## Saves this Mermaid chart to a valid file.
## Note: Subgraphs are not saved to file automatically and should be saved individually.
func save_to_file(path: String):
	var file = FileAccess.open(path, FileAccess.WRITE)
	logger.debug("Saving to file: " + path)
	if file == null:
		logger.error("File $s failed to open!" % [path])
		return false
	
	var text = "# Mermaid Graph\n```mermaid\n" + compose() + "\n```\n"
	file.store_string(text)


## Finds a Mermaid chart within the Markdown string and returns a MermaidParser instance loaded from the chart.
static func from_markdown(markdown: String) -> MermaidParser:
	var text := _strip_markdown(markdown)
	if is_string_valid_mermaid(text):
		return MermaidParser.new(text)
	
	else:
		Print.from("MERMAID_PARSER", "String did not contain valid Mermaid!", Print.ERROR)
		return null


## Parses Mermaid content and populates the nodes and links of the Mermaid object.
##
## Expected content format: 
## flowchart TB
## A-->B
## B-->C
## %% This is a comment
## %% This is another comment
## 
## B-->D
func parse(content: String):
	# Create a dictionary to store original strings and their placeholders
	var placeholders = {}
	logger.info("Parsing Mermaid File:\n" + content)
	
	# Use a regular expression to find all quoted strings
	var regex = RegEx.new()
	regex.compile("\"([^\"]*)\"") # Matches any string enclosed in double quotes
	var results = regex.search_all(content)
	
	# Replace each quoted string with a unique placeholder
	for result in results:
		var original_string = result.get_string()
		var placeholder = "PLACEHOLDER_" + str(result.get_start())
		content = content.replace(original_string, placeholder)
		placeholders[placeholder] = original_string
		logger.verbose("Substring " + original_string + " replaced with placeholder " + placeholder)
	logger.debug("Finished Regex Replacement.")
	
	# Split the flowchart string into individual lines
	var lines := Array(content.split("\n")).filter(func(x): return x.strip_edges() != "") # Ignore empty parts after split
	logger.debug("Split flowchart into %d lines." % [lines.size()])
	
	# Loop through the other lines. Ignore lines that are blank or start with %% (which is a comment)
	for i in lines.size():
		var line: String = lines[i].strip_edges()
		
		# Ignore comments and empty lines
		if line == "" or line.begins_with("%%"):
			continue
		
		# Read line 1. Line should be "flowchart XX" where XX is a valid flowchart orientation (LR, TD, etc.)
		elif line.begins_with("flowchart") or line.begins_with("graph"):
			orientation = line.split(" ")[1]
			logger.debug("Found graph orientation: " + line)
		
		# Parse the line
		else:
			_parse_line(line)
	
	if active_subgraph:
		logger.error("Subgraph %s was never closed with an ''end'' statement!")
		# Gracefully close our subgraph.
		
	# At the end of the parse function, replace placeholders back to original strings
	if placeholders.size() > 0:
		logger.debug("Replacing %d placeholders with text." % [placeholders.size()])
		for node in shared_nodes.values():
			logger.assert_that(node != null, "Null node was added to the graph!")
			for placeholder in placeholders.keys():
				node.raw_text = node.raw_text.replace(placeholder, placeholders[placeholder])
				node.id = node.id.replace(placeholder, placeholders[placeholder])
			for link in node.links:
				for placeholder in placeholders.keys():
					link.raw_note = link.raw_note.replace(placeholder, placeholders[placeholder])
	
	# Pre-process any nodes that need it.
	_initialize_nodes()
	
	logger.debug("Mermaid Parse Completed.\n")


## Composes a Mermaid flowchart string based on the current nodes and links.
func compose() -> String:
	var result = "flowchart " + orientation + _compose_subgraph()
	return result


## Composes this flowchart as a subgraph.
func _compose_subgraph(processed_links: Array = [], detailed_nodes: Array = []) -> String:
	var result := ""
	var processed_nodes: Array = []  # List of nodes that have already been processed
	logger.verbose("Composing flowchart")
	
	for node_id in nodes:
		var node: MermaidNode = nodes[node_id]
		logger.verbose("Composing node %s" % [node_id])

		if node.type == MermaidNode.NodeType.INLINE and !detailed_nodes.has(node.id):
			logger.verbose("Entering subgraph %s" % [node_id])
			result += "\nsubgraph %s%s\nend" % [node.id, node.mermaid._compose_subgraph(processed_links, detailed_nodes)]
			if !processed_nodes.has(node.id):
				processed_nodes.append(node.id)
			if !detailed_nodes.has(node.id):
				detailed_nodes.append(node_id)
		
		# Skip nodes that have already been processed and have no outgoing links
		if node_id in processed_nodes and node.links.size() == 0:
			logger.verbose("Node %s has already been composed, skipping." % [node_id])
			continue
		
		# If the node has links, create a line for each link
		if node.links.size() > 0:
			for link in node.links:
				# If this link has already been processed (incl. in the reverse direction), skip it
				if [link.node.id, node_id] in processed_links or [node_id, link.node.id] in processed_links:
					logger.verbose("Link to %s has already been composed, skipping." % [link.node.id])
					continue
				
				# If this link was added by a node during runtime, don't write it to file.
				if link.type == MermaidLink.LinkType.DYNAMIC:
					logger.verbose("Link to %s is dynamically generated, skipping." % [link.node.id])
					continue
				
				if !nodes.has(link.node.id):
					logger.verbose("Link to %s skipped, target node is not a part of graph." % [link.node.id])
					continue
				
				# If the node has been printed with details, print just the node_id
				logger.verbose("Composing link to %s" % [link.node.id])
				if link.node.type == MermaidNode.NodeType.INLINE:
					result += "\n" + node.compose(!detailed_nodes.has(node_id)) + link.compose() + \
							link.node.compose(false)
				else:
					result += "\n" + node.compose(!detailed_nodes.has(node_id)) + link.compose() + \
							link.node.compose(!detailed_nodes.has(link.node.id))
				
				# Mark both start and end nodes as processed
				if !processed_nodes.has(node_id):
					processed_nodes.append(node_id)
				if !processed_nodes.has(link.node.id):
					processed_nodes.append(link.node.id)
				if !detailed_nodes.has(node_id) and node.type != MermaidNode.NodeType.INLINE:
					detailed_nodes.append(node_id)
				if !detailed_nodes.has(link.node.id) and link.node.type != MermaidNode.NodeType.INLINE:
					detailed_nodes.append(link.node.id) 
				
				# Add this link to the processed links
				processed_links.append([node_id, link.node.id])
		
		if node_id not in processed_nodes and node.type != MermaidNode.NodeType.INLINE:
			# If the node has no links, simply print the node_id or detailed node.
			logger.verbose("Node %s has no valid outgoing links printing details only." % [node_id])
			result += "\n" + node.compose()
		
		# Add node to processed nodes
		processed_nodes.append(node_id)
	
	logger.info("Composed Mermaid File:\n" + result)
	return result


# Parses a string containing one or more chains.
func _parse_line(line: String):
	# Handle exiting a subgraph node.
	if line.begins_with("end"):
		if !active_subgraph:
			logger.error("end statement encountered. This is a reserved keyword, the graph will fail to parse!")
		else:
			if active_subgraph.active_subgraph:
				# We have at least one level of nested subgraph. Let the lower level handle this.
				active_subgraph._parse_line(line)
			else:
				active_subgraph._initialize_nodes()
				active_subgraph = null
				logger.verbose("Exiting subgraph.")
			return
	
	if active_subgraph:
		logger.debug("Passing line \"%s\" to subgraph." % [line])
		active_subgraph._parse_line(line)
		return
	
	logger.debug("Parsing line: " + line)
	# Special case: Subgraphs are a node that contains a graph.
	if line.begins_with("subgraph"):
		# Get the name of the subgraph. Assumes that there is only one space.
		var id = line.get_slice(" ", 1)
		if shared_nodes.has(line):
			logger.error(line + " has already been defined!")
			
		var old_node = null
		if shared_nodes.has(id):
			old_node = shared_nodes[id]
		var subgraph_node = _create_node(id, "", MermaidNode.NodeType.INLINE)
		if old_node:
			_replace_node(old_node, subgraph_node)
		active_subgraph = subgraph_node.mermaid
		active_subgraph.orientation = orientation
		active_subgraph.shared_nodes = shared_nodes
		return
	
	# Split the line into individual chains based on arrow operators
	var chains = MermaidLink.split_line(line)
	var node_ids = chains.map(func(subarray): return subarray[0])
	var node_links = chains.map(func(subarray): return subarray[1])

	for i in range(node_ids.size()):
		var nodes_in_chain = Array(node_ids[i].split("&")).filter(func(x): return x.strip_edges() != "") # Ignore empty parts after split
		var link_string = node_links[i].strip_edges() if node_links[i].strip_edges() != "" else ""
		var link_properties = MermaidLink.parse_link(link_string)

		for node_id in nodes_in_chain:
			var node = _get_or_create_node(node_id.strip_edges())
			# If there is a next chain, add a link to all its nodes
			if i < node_ids.size() - 1:
				var nodes_in_next_chain = Array(node_ids[i + 1].split("&")).filter(func(x): return x.strip_edges() != "") # Ignore empty parts after split
				for next_node_id in nodes_in_next_chain:
					var next_node = _get_or_create_node(next_node_id.strip_edges())
					node.add_link(next_node, link_properties["note"], link_properties["type"], link_properties["direction"], link_properties["arrow"], link_properties["length"])


## Retrieves a node from the nodes dictionary if it exists,
## otherwise creates a new node, adds it to the dictionary, and returns it.
func _get_or_create_node(text: String) -> MermaidNode:
	var node_values = MermaidNode.parse_node(text)
	var node_id: String = node_values[0]
	var node_type: MermaidNode.NodeType = node_values[1]
	var node_text: String = node_values[2]
	
	var node: MermaidNode
	if shared_nodes.has(node_id):
		node = shared_nodes[node_id]
		# Add to current graph even if this was originally created in a subgraph.
		nodes[node_id] = node
		
		logger.verbose("Searching for node. Node " + node_id + " exists.")
		# Update the text (and type) if we are defining it after the node was first introduced.
		if node_text != "":
			if node.raw_text != "" and node.raw_text != node_text:
				logger.warning("Node %s had its text overwritten! The previous message will be lost. Old text: %s New text: %s" % [node_id, node.raw_text, node_text])
			if node.type != node_type and node.type != node_type:
				if node.type != MermaidNode.NodeType.STANDARD:
					logger.warning("Node %s has changed type! Old type: %s New type: %s" % [node_id, node.type, node_type])
				# Create a new node of the correct type
				var new_node = _create_node(node_id, node_text, node_type)
				# Replace the old node with the new node in all nodes and links
				_replace_node(node, new_node)
				# Use the new node from now on
				node = new_node
			else:
				node.raw_text = node_text
				node.type = node_type
	else:
		# Use the appropriate node type to allow for diversity in node traversal.
		logger.verbose("Searching for node. Node " + node_id + " does not exist. Creating node.")
		node = _create_node(node_id, node_text, node_type)
	
	return node


## Creates a node of the appropriate type.
func _create_node(node_id: String, node_text: String, node_type: MermaidNode.NodeType) -> MermaidNode:
	var node: MermaidNode
	# Use the appropriate node type to allow for diversity in node traversal.
	logger.verbose("Creating node %s of type %s with text \"%s\"." % [node_id, MermaidNode.NodeType.keys()[node_type], node_text] )
	match node_type:
		MermaidNode.NodeType.STANDARD:
			node = MermaidNode.new(node_id, node_text, node_type)
		MermaidNode.NodeType.DIALOGUE:
			node = MermaidDialogueNode.new(node_id, node_text)
		MermaidNode.NodeType.COMMAND:
			node = MermaidCommandNode.new(node_id, node_text)
		MermaidNode.NodeType.SUBGRAPH, MermaidNode.NodeType.INLINE:
			node = MermaidSubgraphNode.new(node_id, node_text, node_type)
		MermaidNode.NodeType.ROUNDED:
			logger.warning("Node type ROUNDED does not do anything! TODO: Override node type. Maybe use a different node for now?")
			node = MermaidUndefinedNode.new(node_id, node_text, node_type)
		MermaidNode.NodeType.STADIUM:
			logger.warning("Node type STADIUM does not do anything! TODO: Override node type. Maybe use a different node for now?")
			node = MermaidUndefinedNode.new(node_id, node_text, node_type)
		MermaidNode.NodeType.CYLINDRICAL:
			logger.warning("Node type CYLINDRICAL does not do anything! TODO: Override node type. Maybe use a different node for now?")
			node = MermaidUndefinedNode.new(node_id, node_text, node_type)
		MermaidNode.NodeType.CIRCULAR:
			logger.warning("Node type CIRCULAR does not do anything! TODO: Override node type. Maybe use a different node for now?")
			node = MermaidUndefinedNode.new(node_id, node_text, node_type)
		MermaidNode.NodeType.HEXAGON:
			logger.warning("Node type HEXAGON does not do anything! TODO: Override node type. Maybe use a different node for now?")
			node = MermaidUndefinedNode.new(node_id, node_text, node_type)
		MermaidNode.NodeType.PARALLELOGRAM:
			logger.warning("Node type PARALLELOGRAM does not do anything! TODO: Override node type. Maybe use a different node for now?")
			node = MermaidUndefinedNode.new(node_id, node_text, node_type)
		MermaidNode.NodeType.PARALLELOGRAM2:
			logger.warning("Node type PARALLELOGRAM2 does not do anything! TODO: Override node type. Maybe use a different node for now?")
			node = MermaidUndefinedNode.new(node_id, node_text, node_type)
		MermaidNode.NodeType.TRAPEZOID:
			logger.warning("Node type TRAPEZOID does not do anything! TODO: Override node type. Maybe use a different node for now?")
			node = MermaidUndefinedNode.new(node_id, node_text, node_type)
		MermaidNode.NodeType.TRAPEZOID2:
			logger.warning("Node type TRAPEZOID2 does not do anything! TODO: Override node type. Maybe use a different node for now?")
			node = MermaidUndefinedNode.new(node_id, node_text, node_type)
		MermaidNode.NodeType.DOUBLECIRCLE:
			logger.warning("Node type DOUBLECIRCLE does not do anything! TODO: Override node type. Maybe use a different node for now?")
			node = MermaidUndefinedNode.new(node_id, node_text, node_type)
		MermaidNode.NodeType.FLAG:
			logger.warning("Node type FLAG does not do anything! TODO: Override node type. Maybe use a different node for now?")
			node = MermaidUndefinedNode.new(node_id, node_text, node_type)
		_:
			logger.assert_that(false, "Attempted to create a node of unknown type! " + str(node_type))
			node = MermaidUndefinedNode.new(node_id, node_text, node_type)
	nodes[node_id] = node
	shared_nodes[node_id] = node
	return node


## Searches through all nodes and links and replaces references to old with new.
func _replace_node(old: MermaidNode, new: MermaidNode):
	# Copy over settings from the old node.
	new.links = old.links
	
	# Iterate over all nodes in the nodes dictionary
	for node_id in nodes.keys():
		# If the node is the old node, replace it with the new node
		if nodes[node_id] == old:
			nodes[node_id] = new
			logger.verbose("Replaced node with id %s in nodes dictionary." % node_id)
			
		# Iterate over all links in the node
		for i in range(nodes[node_id].links.size()):
			var link = nodes[node_id].links[i]
			# If the link's node is the old node, replace it with the new node
			if link.node == old:
				link.node = new
				logger.verbose("Replaced node in link from %s to %s." % [nodes[node_id].id, link.node.id])


## Set up hooks and trigger any special conditions for nodes.
func _initialize_nodes():
	if nodes.size() > 0:
		start_node = nodes.values()[0]
	
	for node in nodes.values():
		match node.type:
			MermaidNode.NodeType.COMMAND:
				if node.id.to_lower().contains("start"):
					start_node = node
				elif node.id.to_lower().contains("exit"):
					exit_nodes.append(node)
			MermaidNode.NodeType.SUBGRAPH, MermaidNode.NodeType.INLINE:
				node.base_dir = file_path.get_base_dir()
				node.ready()


## Takes a Markdown file containing Mermaid and returns only the Mermaid code itself.
static func _strip_markdown(text: String) -> String:
	var mermaid_code = ""
	var is_mermaid_code := false
	var mermaid_found := false
	Print.from("MERMAID_PARSER", "Stripping markdown from file:\n" + text)
	
	for line in text.split("\n"):
		if line.strip_edges() == "```mermaid":
			is_mermaid_code = true
			mermaid_found = true
			continue
		if line.strip_edges() == "```":
			is_mermaid_code = false
			continue
		if is_mermaid_code:
			mermaid_code += line + "\n"
	
	if mermaid_found:
		return mermaid_code.strip_edges()
	else:
		return text.strip_edges()
