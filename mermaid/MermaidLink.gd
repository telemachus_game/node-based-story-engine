## A class representing a link between nodes in a mermaid diagram.
extends Resource
class_name MermaidLink
	
## Enum representing the direction of the link.
enum LinkDirection {
	OPEN,            # Link does not have a direction (---).
	UNIDIRECTIONAL,  # Link is directed from one node to another (-->).
	BIDIRECTIONAL,   # Link is bidirectional between two nodes (<-->.
}

## Enum representing the type of the link line.
enum LinkType {
	SOLID,     # Solid line: --- 
	DOTTED,    # Dotted line: -.-
	THICK,     # Thick line: ==>
	INVISIBLE, # Invisible line: ~~~
	DYNAMIC,   # Lines added during runtime
}

## Enum representing the type of arrow for the link.
enum ArrowType {
	ARROW,   # Open arrow: >
	SOLID,   # Closed arrow: o
	CROSS,   # Cross arrow: x
	OPEN,    # No arrow, -, =
}

## The direction of the link.
var direction: LinkDirection

## The type of the link line.
var type: LinkType

## The type of arrow used in the link.
var arrow: ArrowType

## The length of the link. Default is 2.
var length: int


## Optional note or text on the link.
## Text has parentheses and text newlines removed.
var note: String:
	get:
		return raw_note.replace("\"", "").replace("\\n", "\n")

## Optional note or text on the link. Text has newlines and parenteses left in.
var raw_note: String


## The target node the link is pointing to.
var node: MermaidNode

## Initializes a new link with given parameters.
func _init(link_direction: LinkDirection, link_type: LinkType, link_arrow: ArrowType, link_length: int, link_note: String, link_node: MermaidNode):
	self.direction = link_direction
	self.type = link_type
	self.arrow = link_arrow
	self.length = link_length
	self.raw_note = link_note
	self.node = link_node


## Compose this link into a string.
func compose() -> String:
	# Define link components based on link properties
	var link_type: String
	match type:
		LinkType.SOLID:
			link_type = "-"
		LinkType.DOTTED:
			link_type = "."
		LinkType.THICK:
			link_type = "="
		LinkType.INVISIBLE:
			link_type = "~"

	var link_arrow: Array
	match arrow:
		ArrowType.ARROW:
			link_arrow = [">", "<"]
		ArrowType.SOLID:
			link_arrow = ["o", "o"]
		ArrowType.CROSS:
			link_arrow = ["x", "x"]
		ArrowType.OPEN:
			link_arrow = [link_type, link_type] # Get last character of link type

	# Assemble link string
	var link_string: String = ""

	# If link is bidirectional, start with arrow
	if direction == LinkDirection.BIDIRECTIONAL:
		link_string += link_arrow[1]

	# If note is present, include it in the link
	if raw_note != "":
		# Handle special case of dotted link type when note is present
		if type == LinkType.DOTTED:
			link_string += "-."
		else:
			link_string += link_type + link_type

		link_string += " " + raw_note + " "
	
	# Add length to the link
	if length > 1:
		for i in range(length - 1):
			link_string += link_type
	
	# Add ending part of the link
	if type == LinkType.DOTTED:
		link_string += ".-"
	else:
		link_string += link_type + link_type

	# Finish with arrow
	link_string += link_arrow[0]
	
	return link_string


## Parses a string containing one or more chains.
## The string can be one of the following formats:
## "A-->B"
## "A-->B-->C"
## "A-->B & C-->D"
# Helper function to parse a link string and return its properties as a dictionary.
static func parse_link(link: String) -> Dictionary:
	var properties = {}
	link =  link.strip_edges()
	
	# Extract note
	var note_regex = RegEx.new()
	note_regex.compile("(?<=[-=~.>ox|])(\\s?[^-=~.|>\\n]{2,}\\s?)(?=[-=~.|])")
	var regex_match = note_regex.search(link)
	if regex_match:
		properties["note"] = regex_match.get_string().strip_edges()
	else:
		properties["note"] = ""  # If no note is found, initialize as empty
	
	# Remove end note (if it exists) so the link ends with the arrow.
	link = link.get_slice("|", 0).strip_edges()
	
	# Check link type
	if link.contains("~~~"):
		properties["type"] = MermaidLink.LinkType.INVISIBLE
	elif link.contains(".-"):
		properties["type"] = MermaidLink.LinkType.DOTTED
	elif link.contains("=="):
		properties["type"] = MermaidLink.LinkType.THICK
	else:
		properties["type"] = MermaidLink.LinkType.SOLID
	
	# Calculate length
	var length_regex = RegEx.new()
	length_regex.compile("[-=~.]{2,}")
	var regex_matches := length_regex.search_all(link)
	if !regex_matches.is_empty():
		regex_match = regex_matches[-1]
		var link_length = regex_match.get_string().length()
		# If the link doesn't end with an arrow, an extra character is part of the line
		if not (link.ends_with(">") or link.ends_with("o") or link.ends_with("x")):
			link_length -= 1
		if properties["type"] == MermaidLink.LinkType.DOTTED and regex_match.strings[0].count("-") > 1:
			link_length -= 1
		properties["length"] = link_length - 1  # Subtract 1 to account for base length
	else:
		properties["length"] = 1
	
# Check for arrow type
	if link.ends_with(">"):
		properties["arrow"] = MermaidLink.ArrowType.ARROW
	elif link.ends_with("x"):
			properties["arrow"] = MermaidLink.ArrowType.CROSS
	elif link.ends_with("o"):
		properties["arrow"] = MermaidLink.ArrowType.SOLID
	else:
		properties["arrow"] = MermaidLink.ArrowType.OPEN
	
	# Check for link direction
	if link.begins_with("<") or link.begins_with("o") or link.begins_with("x"):
		properties["direction"] = MermaidLink.LinkDirection.BIDIRECTIONAL
	elif link.ends_with(">") or link.ends_with("o") or link.ends_with("x"):
			properties["direction"] = MermaidLink.LinkDirection.UNIDIRECTIONAL
	else:
		properties["direction"] = MermaidLink.LinkDirection.OPEN
		
	return properties


# Splits a line on one or more valid links.
static func split_line(line: String) -> Array:
	var nodes_and_arrows = []
	var regex = RegEx.new()
	regex.compile("((?:(?:\\s+[ox])?|<)(?<![~=.-])[~=.-][~=.-](?:(?:[^>xo=~-][^~=.-]+[~=.-]+[~\\-=>ox]\\s*)|(?:[~=.-]*[=~xo>-]))(?:\\s?\\|.[^|]*\\|\\s?)?)")
	
	# Initiate a variable to hold the start of the next substring to extract
	var start = 0

	for result in regex.search_all(line):
		var up_node = line.substr(start, result.get_start() - start).strip_edges()
		var link = line.substr(result.get_start(), result.get_end() - result.get_start()).strip_edges()
		start = result.get_end()

		# Ignore empty strings
		if up_node != "":
			nodes_and_arrows.append([up_node, link])

	# Add the final node
	var down_node = line.substr(start, line.length() - start).strip_edges()
	if down_node != "":
		nodes_and_arrows.append([down_node, ""])

	return nodes_and_arrows
