extends Resource
class_name StoryInterface


# Private variables.
var _mermaid: MermaidParser
var _current_node: MermaidNode
var logger: Logger	


func _init(mermaid_parser: MermaidParser):
	logger = Print.create_logger("STORY_ENGINE", Print.INFO, Print.VERBOSE)
	logger.info("Initialized story engine.")
	_mermaid = mermaid_parser
	_current_node = _mermaid.start_node
	while _current_node.can_auto_advance:
		_current_node = _current_node.auto_advance()


# The text displayed by the current node. In some situations, it may be blank.
var text: String:
	get:
		return _current_node.get_text()


# A string array containing.
var options: Array:
	get:
		var link_options = _current_node.get_link_options()
		logger.verbose("Available connections: %s" % [link_options])
		return link_options


# Move forward to the next node in the story.
# If multiple options are available, select the one you want with index.
func advance(index := 0):
	logger.assert_that(index < options.size(), \
			"The story cannot advance to link #%d because the current node only has %d links to chose from!" % [index, options.size() - 1])
	_current_node = _current_node.get_next_node(index)
	logger.verbose("Advanced to node %s" % _current_node.id)
	
	# If this node is auto-advancing, then move it forward automatically until you hit stopping point.
	while _current_node.can_auto_advance:
		_current_node = _current_node.auto_advance()
		logger.verbose("Auto-Advanced to node %s" % _current_node.id)
		
